# Folder structure:

## config:

Chứa các file json lưu biến cấu hình hệ thống.

## src:

Toàn bộ source code của phần backend.

### constant:

Chứa các biến const được sử dụng trong cả project.

### cors:

Sử dụng như một middleware của project.

### routers:

Mỗi thư mục route sẽ chứa cả 2 file route và controller

### services:

database...

### upload:

Lưu static file để sử dụng public.

### validation:

Chứa các route validation.