import config from 'config';

import {Pool, PoolClient} from 'pg';

class DbSingleton {
  private static instance: DbSingleton;
  private readonly pool: Pool;

  /**
   * The Singleton's constructor should always be private to prevent direct
   * construction calls with the `new` operator.
   */
  private constructor() {
    if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'staging') {
      this.pool = new Pool(config.get(`${process.env.NODE_ENV}.database`));
    } else {
      const connectionString: string | undefined = config.get(`${process.env.NODE_ENV}.database.url`);
      this.pool = new Pool({connectionString});
    }
  }

  /**
   * The static method that controls the access to the singleton instance.
   *
   * This implementation let you subclass the Singleton class while keeping
   * just one instance of each subclass around.
   */
  public static getInstance(): DbSingleton {
    if (!DbSingleton.instance) {
      DbSingleton.instance = new DbSingleton();
    }
    return DbSingleton.instance;
  }

  /**
   * Execute query without a client
   * @param query
   * @param parameters
   */
  private async queryNonClient(query: string, parameters?: any[]) {
    return this.pool.query(query, parameters);
  }

  /**
   * Execute query
   * @param query
   * @param parameters
   * @param inputClient
   */
  async query(query: string, parameters: any[] = [], inputClient?: PoolClient) {
    if (!inputClient) return await this.queryNonClient(query, parameters);
    return inputClient.query(query, parameters);
  }

  /**
   * Get pool for create client, do transaction
   */
  getPool(): Pool {
    return this.pool;
  }
}

export default DbSingleton;
