import passport from "passport";
import {generateJWT, IAccount, validPassword} from '../models/IAccount';
import {Strategy as LocalStrategy} from "passport-local";
import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt';
import {Request, Response, NextFunction} from "express";
import httpResponseHandler from "../cors/httpResponseHandler";
import AuthenticationServices from "../services/commonServices/AuthenticationServices";
import DatabaseCommand from "../utils/DatabaseCommand";
import UserServices from "../services/public/UserServices";
import CompanyServices from "../services/public/CompanyServices";
import {IBusinessContract} from "../models/IBusinessContract";

passport.use('local', new LocalStrategy(async (username: string, password: string, done: CallableFunction) => {
  const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const splitFromUsername = username.split('@');
      const companyCode = splitFromUsername[1] ? splitFromUsername[1].toUpperCase() : '';
      const availableCompanies = await CompanyServices.getCompanies([{
        code: companyCode, statusid: 2
      }], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!availableCompanies.length) return done(null, false, `Company has code ${companyCode} not found or unavailable.`);
      const businessContract = await AuthenticationServices.checkBusinessContractOfAvailableCompany(availableCompanies[0], client);
      if (!businessContract) {
        await DatabaseCommand.commitTransaction(client);
        return done(null, false, `Company has code ${splitFromUsername[1]} not have a valid business contract.`);
      }
      const account = await AuthenticationServices.getAccountByUsernameAndCompanyId(username, businessContract.companyid, DatabaseCommand.lockStrength.SHARE, client);
      if (!account) {
        await DatabaseCommand.commitTransaction(client);
        return done(null, false, `Account has username ${username} not found or unavailable.`);
      }
      if (validPassword(password, account)) {
        await DatabaseCommand.commitTransaction(client);
        return done(null, {account, businessContract});
      }
      await DatabaseCommand.commitTransaction(client);
      return done(null, false, `Incorrect password.`);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return done(e, false);
    } finally {
      client.release();
    }
  }
));

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET
};

passport.use('jwt', new JwtStrategy(jwtOptions, async (jwt_payload, done) => {
  const client = await DatabaseCommand.newClient();
  try {
    await DatabaseCommand.beginTransaction(client);
    const companyId = jwt_payload.companyid;
    const availableCompanies = await CompanyServices.getCompanies([{
      id: companyId, statusid: 2
    }], DatabaseCommand.lockStrength.KEY_SHARE, client);
    if (!availableCompanies.length) return done(null, false, `Company has id ${companyId} not found or unavailable.`);
    const businessContract = await AuthenticationServices.checkBusinessContractOfAvailableCompany(availableCompanies[0], client);
    if (!businessContract) {
      await DatabaseCommand.commitTransaction(client);
      return done(null, false, `Company has id ${companyId} not have a valid business contract.`);
    }
    const account = await AuthenticationServices.getAccountByIdAndCompanyId(jwt_payload.id, businessContract.companyid, DatabaseCommand.lockStrength.KEY_SHARE, client);
    if (!account) {
      await DatabaseCommand.commitTransaction(client);
      return done(null, false, `Account has id ${jwt_payload.id} not found or unavailable.`);
    }
    await DatabaseCommand.commitTransaction(client);
    return done(null, {account, businessContract});
  } catch (e) {
    console.log(e);
    await DatabaseCommand.rollbackTransaction(client);
    return done(e, false);
  } finally {
    client.release();
  }
}));

export const passportJwt = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('jwt', async (error, account, info) => {
    if (error) return httpResponseHandler.responseErrorToClient(res, 500);
    if (account) {
      req.user = account;
      next();
    } else httpResponseHandler.responseErrorToClient(res, 401, info);
  })(req, res, next);
}

export const passportLocal = (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('local', async (error, account, info) => {
    if (error) return res.status(500).json({messages: 'Something wrong.'});
    if (account) {
      const thisAccount = account.account as IAccount;
      const thisBusinessContract = account.businessContract as IBusinessContract;
      const client = await DatabaseCommand.newClient();
      try {
        await DatabaseCommand.beginTransaction(client);
        const users = await UserServices.getUsers([{
          id: thisAccount.id, companyid: thisAccount.companyid, statusid: 2
        }], DatabaseCommand.lockStrength.KEY_SHARE, client);
        if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${thisAccount.userid} and companyid ${thisAccount.companyid} not found or unavailable.`);
        const user = users[0] as any;
        const roles = await AuthenticationServices.getRolesAndItsPermissionsOfUser(user, thisBusinessContract.typeid, DatabaseCommand.lockStrength.KEY_SHARE, client);
        const token = generateJWT(thisAccount);
        await DatabaseCommand.commitTransaction(client);
        return httpResponseHandler.responseToClient(res, 200, {token, roles});
      } catch (e) {
        console.log(e);
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseToClient(res, 500);
      } finally {
        client.release();
      }
    } else return httpResponseHandler.responseErrorToClient(res, 401, info);
  })(req, res, next);
}
