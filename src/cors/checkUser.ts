import {Request, Response, NextFunction} from "express";
import UserServices from "../services/public/UserServices";
import httpResponseHandler from "./httpResponseHandler";
import DatabaseCommand from "../utils/DatabaseCommand";
import AuthenticationServices from "../services/commonServices/AuthenticationServices";

/**
 * This middleware check user for specific route
 * @param req
 * @param res
 * @param next
 */
export default async function checkUser(req: Request, res: Response, next: NextFunction) {
  try {
    if (!req.user) return httpResponseHandler.responseErrorToClient(res, 400);
    const user = req.user as any;
    const { account, businessContract } = user;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const users = await UserServices.getUsers([{id: account.userid, companyid: account.companyid, statusid: 2}], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${account.userid} not found or unavailable.`);
      // const canAccessApi = await AuthenticationServices.checkPermission(users[0], businessContract.typeid, req.originalUrl, req.method, 'SA', client);
      const canAccessApi = true;
      if (canAccessApi) {
        req.user = users[0];
        return next();
      }
      return httpResponseHandler.responseErrorToClient(res, 403, `You do not have permission to access this api.`);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  } catch (e) {
    console.log(e);
    return httpResponseHandler.responseErrorToClient(res, 500);
  }
}
