import httpResponse from '../constant/HttpResponse';
import {Response} from 'express';

export default {
  responseErrorToClient: function (res: Response, status: number, message: string = ''): Response {
    if (message) return res.status(status).json({message: message});
    const response = httpResponse.find(item => item.status === status);
    if (response) return res.status(response.status).json({message: response.message});
    return res.status(status);
  },
  responseToClient: function (res: Response, status: number, body: any = null): Response {
    if (body) return res.status(status).json(body);
    const response = httpResponse.find(item => item.status === status);
    if (response) return res.status(response.status).json({message: response.message});
    return res.status(status);
  }
}
