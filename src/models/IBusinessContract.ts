export interface IBusinessContract {
  id: number;
  companyid: number;
  start: Date;
  expired: Date;
  statusid: number;
  statuscode: string;
  typeid: number;
  price: number;
  currency: string;
  quantity: number;
  total: number;
  totalbranch: number;
  isprimary: boolean;
}
