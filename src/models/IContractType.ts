export interface IContractType {
  id: number;
  code: string;
  price: number;
  unit: string;
  lastmodified: Date;
  description: string;
  totalusers: number;
  totalbranches: number;
  currency: string;
}
