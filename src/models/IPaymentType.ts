export interface IPaymentType {
  id: number;
  code: string;
  name: string;
}
