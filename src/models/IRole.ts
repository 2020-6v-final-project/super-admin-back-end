export interface IRole {
  id: number;
  code: string;
  name: string;
  description: string;
  isprimary: boolean;
}
