export interface IStatus {
  id: number;
  code: string;
  name: string;
}
