import UtilityFunctions from '../utils/Functions';

export interface IUser {
  id: number,
  firstname: string,
  middlename: string,
  lastname: string,
  email: string,
  dateofbirth: Date,
  address: string,
  phone: string,
  ismale: boolean,
  joinedat: Date,
  validatetoken: string,
  validatetokenexpiredat: Date,
  statusid: number;
  statuscode: string
  companycode: string;
  companyid: number;
  imgpath: string;
}

// export type ICreateUser = Omit<IUser, 'id' | ''>

/**
 * Get validate token to create account
 * @param user
 */
export function getValidateToken(user: IUser): IUser {
  user.validatetoken = UtilityFunctions.randomString(6);
  const expiry = new Date();
  //Can use in one week
  expiry.setDate(expiry.getDate() + 7);
  user.validatetokenexpiredat = expiry;
  return user;
}
