import {Application} from 'express';
import AuthenticationRoutes from "./routes/authenticationRoutes/AuthenticationRoutes";
import PermissionRoutes from "./routes/adminRoutes/permissionsRoutes/PermissionRoutes";
import ApiRoutes from "./routes/adminRoutes/apisRoutes/ApiRoutes";
import UserRoutes from "./routes/adminRoutes/usersRoutes/UserRoutes";
import CompanyRoutes from "./routes/adminRoutes/companiesRoutes/CompanyRoutes";
import RoleRoutes from "./routes/adminRoutes/rolesRoutes/RoleRoutes";
import BusinessContractRoutes from "./routes/adminRoutes/businessContractsRoutes/BusinessContractRoutes";
import ContractTypeRoutes from "./routes/adminRoutes/contractTypesRoutes/ContractTypeRoutes";
import BusinessContractPaymentRoutes
  from "./routes/adminRoutes/businessContractPaymentsRoutes/BusinessContractPaymentRoutes";
import PaymentTypeRoutes from "./routes/adminRoutes/paymentTypeRoutes/PaymentTypeRoutes";
import CurrencyRoutes from "./routes/adminRoutes/currenciesRoutes/CurrencyRoutes";

export default (app: Application) => {
  AuthenticationRoutes(app);
  PermissionRoutes(app);
  ApiRoutes(app);
  UserRoutes(app);
  CompanyRoutes(app);
  RoleRoutes(app);
  BusinessContractRoutes(app);
  BusinessContractPaymentRoutes(app);
  ContractTypeRoutes(app);
  PaymentTypeRoutes(app);
  CurrencyRoutes(app);
};
