import {NextFunction, Request, Response} from "express";
import ApiServices from "../../../services/public/ApiServices";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import PermissionApiServices from "../../../services/public/PermissionApiServices";

export default {
  async getAll(req: Request, res: Response): Promise<Response> {
    try {
      const apis = await ApiServices.getApis([]);
      return httpResponseHandler.responseToClient(res, 200, apis);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async deleteOne(req: Request, res: Response): Promise<Response> {
    const apiId = parseInt(req.params.apiId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const apis = await ApiServices.getApis([{id: apiId}], DatabaseCommand.lockStrength.UPDATE, client);
      if (!apis.length) return httpResponseHandler.responseErrorToClient(res, 404, `Api has id ${apiId} not found.`);
      await Promise.all((await PermissionApiServices.getPermissionApis([{
        apiid: apiId
      }], DatabaseCommand.lockStrength.UPDATE, client)).map(async permissionApi => {
        await PermissionApiServices.deletePermissionApi(permissionApi, client);
      }));
      const result = await ApiServices.deleteApi(apis[0], client);
      if (!result) return httpResponseHandler.responseErrorToClient(res, 500);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async addApi(req: Request, res: Response): Promise<Response> {
    const api = req.body;
    delete api.id;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const createdApi = await ApiServices.createApi(api, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 201, createdApi);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async updateOne(req: Request, res: Response, next: NextFunction) {
    const apiId = parseInt(req.params.apiId);
    const api = req.body;
    delete api.id;
    delete api.route;
    delete api.method;
    delete api.server;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const apis = await ApiServices.getApis([{id: apiId}], DatabaseCommand.lockStrength.NO_KEY_UPDATE);
      if (!apis.length) return httpResponseHandler.responseErrorToClient(res, 404, `Api has id ${apiId} not found.`);
      const thisApi = apis[0];
      Object.assign(thisApi, api);
      await ApiServices.updateApi(thisApi, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
