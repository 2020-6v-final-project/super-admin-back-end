import {Router, Application} from 'express';
import Routes from "../../../constant/Routes";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";
import ApiController from "./ApiController";

const router = Router();

export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, ApiController.getAll)
    .post(passportJwt, checkUser, ApiController.addApi);

  router.route('/:apiId')
    .put(passportJwt, checkUser, ApiController.updateOne)
    .delete(passportJwt, checkUser, ApiController.deleteOne);

  app.use(Routes.ADMIN_APIS, router);
}
