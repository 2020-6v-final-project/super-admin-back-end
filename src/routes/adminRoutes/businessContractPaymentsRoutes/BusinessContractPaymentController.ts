import httpResponseHandler from "../../../cors/httpResponseHandler";
import {NextFunction, Request, Response} from 'express';
import BusinessContractPaymentServices from "../../../services/public/BusinessContractPaymentServices";
import CompanyServices from "../../../services/public/CompanyServices";
import BusinessContractServices from "../../../services/public/BusinessContractServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import StatusServices from "../../../services/public/StatusServices";

export default {

  async getPayments(req: Request, res: Response): Promise<Response> {
    try {
      let payments = await BusinessContractPaymentServices.getBusinessContractPayments([]);
      const {withcontract, withcompany} = req.query;
      if (withcompany) {
        payments = await Promise.all(payments.map(async payment => {
          const tempPayment = payment as any;
          tempPayment.company = (await CompanyServices.getCompanies([{id: payment.companyid}]))[0];
          return tempPayment;
        }));
      }
      if (withcontract) {
        payments = await Promise.all(payments.map(async payment => {
          const tempPayment = payment as any;
          tempPayment.businesscontract = (await BusinessContractServices.getBusinessContracts([{id: payment.businesscontractid, companyid: payment.companyid}]))[0];
          return tempPayment;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, payments);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createPayment(req: Request, res: Response): Promise<Response> {
    const body = req.body;
    delete body.id;
    delete body.createdat;
    delete body.paidat;
    delete body.statusid;
    delete body.statuscode;
    delete body.total
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const contracts = await BusinessContractServices.getBusinessContracts([{
        id: body.businesscontractid, companyid: body.companyid
      }], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!contracts.length) return httpResponseHandler.responseErrorToClient(res, 404, `Business contract has id ${body.businesscontractid} and companyid ${body.companyid} not found.`);
      const thisContract = contracts[0];
      const payment = await BusinessContractPaymentServices.createPaymentForContract(thisContract, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 201, payment)
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getPayment(req: Request, res: Response): Promise<Response> {
    const paymentId = parseInt(req.params.paymentId)
    try {
      const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{id: paymentId}]);
      if (!payments.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment has id ${paymentId} not found.`);
      const payment = payments[0] as any;
      const {withcontract, withcompany} = req.query;
      if (withcompany) {
        payment.company = (await CompanyServices.getCompanies([{id: payment.companyid}]))[0];
      }
      if (withcontract) {
        payment.businesscontract = (await BusinessContractServices.getBusinessContracts([{id: payment.businesscontractid, companyid: payment.companyid}]))[0];
      }
      return httpResponseHandler.responseToClient(res, 200, payment);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async updatePayment(req: Request, res: Response, next: NextFunction) {
    const paymentId = parseInt(req.params.paymentId)
    const body = req.body;
    delete body.id;
    delete body.businesscontract;
    delete body.companyid;
    delete body.createdat;
    delete body.paidat;
    delete body.statusid;
    delete body.statuscode;
    delete body.total
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
        id: paymentId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!payments.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment has id ${paymentId} not found.`);
      const payment = payments[0];
      Object.assign(payment, body);
      const result = await BusinessContractPaymentServices.updateBusinessContractPayment(payment, client);
      if (!result) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async deletePayment(req: Request, res: Response): Promise<Response> {
    const paymentId = parseInt(req.params.paymentId)
    const client = await DatabaseCommand.newClient();
    try {
      const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
        id: paymentId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      if (!payments.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment has id ${paymentId} not found.`);
      const payment = payments[0];
      const deleted = await BusinessContractPaymentServices.deletePayment(payment, client);
      if (!deleted) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async markPaymentAsPaid(req: Request, res: Response, next: NextFunction) {
    const paymentId = parseInt(req.params.paymentId)
    const client = await DatabaseCommand.newClient();
    try {
      const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
        id: paymentId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      if (!payments.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment has id ${paymentId} not found.`);
      const payment = payments[0];
      payment.paidat = new Date();
      const updatedPayment = await BusinessContractPaymentServices.updateBusinessContractPayment(payment, client);
      if (!updatedPayment) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await StatusServices.setStatusCodeToObjects(BusinessContractPaymentServices.IBusinessContractPaymentTableName, 'PAID', [{
        id: payment.id
      }], client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async markPaymentAsDone(req: Request, res: Response, next: NextFunction) {
    const paymentId = parseInt(req.params.paymentId)
    const client = await DatabaseCommand.newClient();
    try {
      const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
        id: paymentId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!payments.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment has id ${paymentId} not found.`);
      const payment = payments[0];
      await StatusServices.setStatusCodeToObjects(BusinessContractPaymentServices.IBusinessContractPaymentTableName, 'DONE', [{
        id: payment.id
      }]);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
