import {Application, Router} from "express";
import Routes from "../../../constant/Routes";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";
import BusinessContractPaymentController from "./BusinessContractPaymentController";

const router = Router();

export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, BusinessContractPaymentController.getPayments)
    .post(passportJwt, checkUser, BusinessContractPaymentController.createPayment);

  router.route('/:paymentId')
    .get(passportJwt, checkUser, BusinessContractPaymentController.getPayment)
    .put(passportJwt, checkUser, BusinessContractPaymentController.updatePayment, BusinessContractPaymentController.getPayment)
    .delete(passportJwt, checkUser, BusinessContractPaymentController.deletePayment);

  router.route('/:paymentId/paid')
    .post(passportJwt, checkUser, BusinessContractPaymentController.markPaymentAsPaid, BusinessContractPaymentController.getPayment);

  router.route('/:paymentId/done')
    .post(passportJwt, checkUser, BusinessContractPaymentController.markPaymentAsDone, BusinessContractPaymentController.getPayment);

  app.use(Routes.ADMIN_BUSINESS_CONTRACT_PAYMENTS, router);
}
