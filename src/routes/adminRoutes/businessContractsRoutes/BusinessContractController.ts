import { Request, Response } from "express";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import BusinessContractServices from "../../../services/public/BusinessContractServices";
import CompanyServices from "../../../services/public/CompanyServices";
import BusinessContractPaymentServices from "../../../services/public/BusinessContractPaymentServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import ContractTypeServices from "../../../services/public/ContractTypeServices";

export default {
  getAllBusinessContract: async function (req: Request, res: Response): Promise<Response> {
    try {
      let businessContractList = await BusinessContractServices.getBusinessContracts([]);
      const {withcompany, withpayments} = req.query;
      if (typeof withpayments !== 'undefined') {
        businessContractList = await Promise.all(businessContractList.map(async contract => {
          const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
            companyid: contract.companyid, businesscontractid: contract.id
          }]);
          const result = contract as any;
          result.businesscontractpayments = payments;
          return result;
        }));
      }
      if (typeof withcompany !== 'undefined') {
        businessContractList = await Promise.all(businessContractList.map(async contract => {
          const companies = await CompanyServices.getCompanies([{id: contract.companyid}]);
          const result = contract as any;
          result.company = companies[0];
          return result;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, businessContractList);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createContract(req: Request, res: Response): Promise<Response> {
    const body = req.body;
    delete body.id;
    delete body.statusid;
    delete body.statuscode;
    delete body.start;
    delete body.expired;
    delete body.price;
    delete body.total;
    delete body.isprimary;
    const client = await DatabaseCommand.newClient();
    try {
      const { withpayments } = req.query;
      await DatabaseCommand.beginTransaction(client);
      const companies = await CompanyServices.getCompanies([{id: body.companyid}], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!companies.length) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${body.companyid} not found.`);
      const contractTypes = await ContractTypeServices.getAllPossibleContractType(DatabaseCommand.lockStrength.SHARE, client);
      const type = contractTypes.find(item => item.id == body.typeid);
      if (!type) return httpResponseHandler.responseErrorToClient(res, 404, `Type has id ${body.typeid} not found.`);
      await Promise.all((await BusinessContractServices.getBusinessContracts([{
        companyid: body.companyid, isprimary: true
      }], DatabaseCommand.lockStrength.UPDATE, client)).map(async contract => {
        contract.isprimary = false; //When add a new business contract, all primary previous contract is set to non primary
        await BusinessContractServices.updateBusinessContract(contract, client);
      }));
      const newBusinessContract = await BusinessContractServices.createBusinessContract({
        ...body, price: type.price //default in db, primary will be true
      }, client);
      await BusinessContractPaymentServices.createPaymentForContract(newBusinessContract, client);
      await DatabaseCommand.commitTransaction(client);
      if(typeof withpayments !== 'undefined') {
        const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
          companyid: newBusinessContract.companyid, businesscontractid: newBusinessContract.id
        }]);
        const result = newBusinessContract as any;
        result.businesscontractpayments = payments;
        return httpResponseHandler.responseToClient(res, 201, result);
      } else return httpResponseHandler.responseToClient(res, 201, newBusinessContract);
    } catch (err) {
      console.log(err)
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
