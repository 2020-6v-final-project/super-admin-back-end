import {Application, Router} from "express";
import {passportJwt} from "../../../config/passport";
import Routes from "../../../constant/Routes";
import checkUser from "../../../cors/checkUser";
import BusinessContractController from "./BusinessContractController";

const router = Router();

export default (app: Application) => {
  router.route('/')
    .get(passportJwt, checkUser, BusinessContractController.getAllBusinessContract)
    .post(passportJwt, checkUser, BusinessContractController.createContract);

  app.use(Routes.ADMIN_BUSINESS_CONTRACTS, router);
}
