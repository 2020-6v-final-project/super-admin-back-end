import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import CompanyServices from "../../../services/public/CompanyServices";
import UserServices from "../../../services/public/UserServices";
import StatusServices from "../../../services/public/StatusServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import AccountServices from "../../../services/public/AccountServices";
import BusinessContractServices from "../../../services/public/BusinessContractServices";
import UserRoleServices from "../../../services/public/UserRoleServices";

export default {
  //SA get list company.
  getAllCompany: async (req: Request, res: Response) => {
    try {
      let companies = await CompanyServices.getCompanies([]);
      const {withowner} = req.query;
      if (typeof withowner !== 'undefined') {
        companies = await Promise.all(companies.map(async company => {
          const returnedCompany = company as any;
          returnedCompany.ownerinfo = (await UserServices.getUsers([{
            id: company.owner, companyid: company.id
          }]))[0];
          return returnedCompany;
        }));
      }
      httpResponseHandler.responseToClient(res, 200, companies);
    } catch (err) {
      console.log(err);
      httpResponseHandler.responseErrorToClient(res, 500);
    }
  },

  getCompanyByCompanyId: async (req: Request, res: Response) => {
    const companyId = parseInt(req.params.companyId);
    try {
      let companies = await CompanyServices.getCompanies([{id: companyId}]);
      if (!companies.length) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${companyId} not found.`);
      const company = companies[0] as any;
      company.ownerinfo = (await UserServices.getUsers([{id: company.owner, companyid: companyId}]))[0];
      return httpResponseHandler.responseToClient(res, 200, company);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },

  addCompany: async (req: Request, res: Response) => {
    const client = await DatabaseCommand.newClient();
    const body = req.body;
    const owner = body.owner;
    delete owner.joinedat;
    delete owner.joinedat;
    delete owner.validatetoken;
    delete owner.validatetokenexpiredat;
    delete owner.statusid;
    delete owner.statuscode;
    delete body.owner;
    delete body.id;
    delete body.statusid;
    delete body.statuscode;
    delete body.numberofusers;
    delete body.numberofbranches;
    delete body.createdat;
    try {
      await DatabaseCommand.beginTransaction(client);
      const companies = await CompanyServices.getCompanies([{
        code: body.code
      }], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (companies.length) return httpResponseHandler.responseErrorToClient(res, 409, `Company has code ${companies[0].code} already exists.`);
      let newCompany = await CompanyServices.createCompany(body, client);
      if (newCompany) {
        await Promise.all([
          UserServices.createSequenceWithCompanyId(newCompany.id, client),
          AccountServices.createSequenceWithCompanyId(newCompany.id, client),
          BusinessContractServices.createSequenceWithCompanyId(newCompany.id, client)
        ]);
        owner.companyid = newCompany.id;
        owner.companycode = newCompany.code;
        const {newUser, newAccount} = await UserServices.createUserWithAccount(owner, client);
        await UserRoleServices.addUserRole({
          userid: newUser.id, companyid: newCompany.id, roleid: 2 //role 2: CA Company Admin
        }, client);
        newCompany.numberofusers = 1;
        newCompany.owner = newUser.id;
        let updatedCompany = await CompanyServices.updateCompany(newCompany, undefined, client);
        if (updatedCompany) {
          const returnCompany = updatedCompany as any;
          returnCompany.owneraccount = newAccount;
          returnCompany.ownerinfo = newUser;
          await DatabaseCommand.commitTransaction(client);
          return httpResponseHandler.responseToClient(res, 201, returnCompany);
        } else {
          await DatabaseCommand.rollbackTransaction(client);
          return httpResponseHandler.responseErrorToClient(res, 400);
        }
      }
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },

  async updateCompany(req: Request, res: Response, next: NextFunction) {
    const companyId = parseInt(req.params.companyId);
    const body = req.body;
    delete body.id;
    delete body.owner;
    delete body.statusid;
    delete body.statuscode;
    delete body.numberofusers;
    delete body.numberofbranches;
    delete body.createdat;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      if (typeof body.code === 'string') {
        if (body.code === '') return httpResponseHandler.responseErrorToClient(res, 400, `Company code can not be blank.`);
        else body.code = body.code.toUpperCase();
      }
      let companies = await CompanyServices.getCompanies([{id: companyId}], DatabaseCommand.lockStrength.UPDATE, client);
      if (!companies.length) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${companyId} not found.`);
      const company = companies[0];
      const oldCode = company.code;
      Object.assign(company, body);
      // if (req.body.owner) targetCompany.owner = req.body.owner;
      const updatedCompany = await CompanyServices.updateCompany(company, oldCode, client);
      if (!updatedCompany) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },

  async markCompanyAsDisabled(req: Request, res: Response, next: NextFunction) {
    const client = await DatabaseCommand.newClient();
    const companyId = parseInt(req.params.companyId);
    try {
      await DatabaseCommand.beginTransaction(client);
      let companies = await CompanyServices.getCompanies([{
        id: companyId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!companies) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${companyId} not found.`);
      const company = companies[0];
      await StatusServices.setStatusCodeToObjects(CompanyServices.ICompanyTableName, "DISABLED", [{id: company.id}], client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },

  async markCompanyAsAvailable(req: Request, res: Response, next: NextFunction) {
    const client = await DatabaseCommand.newClient();
    const companyId = parseInt(req.params.companyId);
    try {
      await DatabaseCommand.beginTransaction(client);
      let companies = await CompanyServices.getCompanies([{
        id: companyId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!companies) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${companyId} not found.`);
      const company = companies[0];
      await StatusServices.setStatusCodeToObjects(CompanyServices.ICompanyTableName, "AVAILABLE", [{id: company.id}], client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async deleteCompany(req: Request, res: Response) {
    const companyId = parseInt(req.params.companyId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const companies = await CompanyServices.getCompanies([{
        id: companyId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      if (!companies.length) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${req.params.companyId} not found.`);
      const company = companies[0];
      company.owner = null;
      const updatedCompany = await CompanyServices.updateCompany(company, undefined, client);
      if (!updatedCompany) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await Promise.all([
        UserServices.dropSequenceWithCompanyId(companyId, client),
        AccountServices.dropSequenceWithCompanyId(companyId, client),
        BusinessContractServices.dropSequenceWithCompanyId(companyId, client)
      ]);
      await CompanyServices.deleteCompany(updatedCompany, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
};
