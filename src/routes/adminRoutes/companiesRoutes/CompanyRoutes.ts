import { Application, Router } from "express";
import { passportJwt } from "../../../config/passport";
import Routes from "../../../constant/Routes";
import checkUser from "../../../cors/checkUser";
import CompanyController from "./CompanyController";
import CompanyUserRoutes from "./usersRoutes/CompanyUserRoutes";
import CompanyBusinessContractRoutes from "./businessContractsRoutes/CompanyBusinessContractRoutes";

const router = Router();

export default (app: Application) => {
  router.route("/")
    .get(passportJwt, checkUser, CompanyController.getAllCompany)
    .post(passportJwt, checkUser, CompanyController.addCompany);

  router.route("/:companyId")
    .get(passportJwt, checkUser, CompanyController.getCompanyByCompanyId)
    .put(passportJwt, checkUser, CompanyController.updateCompany, CompanyController.getCompanyByCompanyId)
    .delete(passportJwt, checkUser, CompanyController.deleteCompany);

  router.route("/:companyId/disabled")
    .post(passportJwt, checkUser, CompanyController.markCompanyAsDisabled, CompanyController.getCompanyByCompanyId);

  router.route("/:companyId/available")
    .post(passportJwt, checkUser, CompanyController.markCompanyAsAvailable, CompanyController.getCompanyByCompanyId);

  CompanyUserRoutes(router);

  CompanyBusinessContractRoutes(router);

  app.use(Routes.ADMIN_COMPANIES, router);
};
