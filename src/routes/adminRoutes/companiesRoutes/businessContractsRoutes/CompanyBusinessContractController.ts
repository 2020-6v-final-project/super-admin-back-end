import {NextFunction, Request, Response} from "express";
import httpResponseHandler from "../../../../cors/httpResponseHandler";
import BusinessContractServices from "../../../../services/public/BusinessContractServices";
import StatusServices from "../../../../services/public/StatusServices";
import DatabaseCommand from "../../../../utils/DatabaseCommand";
import BusinessContractPaymentServices from "../../../../services/public/BusinessContractPaymentServices";
import CompanyServices from "../../../../services/public/CompanyServices";

export default {
  async getBusinessContractById(req: Request, res: Response): Promise<Response> {
    const companyId = parseInt(req.params.companyId);
    const businessContractId = parseInt(req.params.businessContractId);
    try {
      const businessContractList = await BusinessContractServices.getBusinessContracts([{companyid: companyId, id: businessContractId}]);
      if (!businessContractList.length) return httpResponseHandler.responseToClient(res, 404, `Business contract has id ${businessContractId} not found.`);
      const businessContract = businessContractList[0] as any;
      const {withcompany, withpayments} = req.query;
      if (typeof withpayments !== 'undefined') businessContract.payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
            companyid: businessContract.companyid, businesscontractid: businessContract.id
        }]);
      if (typeof withcompany !== 'undefined') businessContract.company = await CompanyServices.getCompanies([{id: businessContract.companyid}]);
      return httpResponseHandler.responseToClient(res, 200, businessContract);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async getBusinessContractsByCompany(req: Request, res: Response): Promise<Response> {
    const companyId = parseInt(req.params.companyId);
    try {
      let businessContractList = await BusinessContractServices.getBusinessContracts([{companyid: companyId}]);
      const {withcompany, withpayments} = req.query;
      if (typeof withpayments !== 'undefined') {
        businessContractList = await Promise.all(businessContractList.map(async contract => {
          const payments = await BusinessContractPaymentServices.getBusinessContractPayments([{
            companyid: contract.companyid, businesscontractid: contract.id
          }]);
          const result = contract as any;
          result.businesscontractpayments = payments;
          return result;
        }));
      }
      if (typeof withcompany !== 'undefined') {
        businessContractList = await Promise.all(businessContractList.map(async contract => {
          const companies = await CompanyServices.getCompanies([{id: contract.companyid}]);
          const result = contract as any;
          result.company = companies[0];
          return result;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, businessContractList);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async markContractAsInvalid(req: Request, res: Response, next: NextFunction) {
    const companyId = parseInt(req.params.companyId);
    const businessContractId = parseInt(req.params.businessContractId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const businessContractList = await BusinessContractServices.getBusinessContracts([{companyid: companyId, id: businessContractId}], DatabaseCommand.lockStrength.UPDATE, client);
      if (businessContractList.length) {
        const businessContract = businessContractList[0];
        await StatusServices.setStatusCodeToObjects(BusinessContractServices.IBusinessContractTableName, "INVALID", [{id: businessContract.id, companyid: companyId}], client);
        await DatabaseCommand.commitTransaction(client);
        return next();
      } else return httpResponseHandler.responseErrorToClient(res, 404);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async markContractAsValid(req: Request, res: Response, next: NextFunction) {
    const companyId = parseInt(req.params.companyId);
    const businessContractId = parseInt(req.params.businessContractId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const businessContractList = await BusinessContractServices.getBusinessContracts([{companyid: companyId, id: businessContractId}], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (businessContractList.length) {
        const businessContract = businessContractList[0];
        await StatusServices.setStatusCodeToObjects(BusinessContractServices.IBusinessContractTableName, "VALID", [{id: businessContract.id, companyid: companyId}], client);
        await DatabaseCommand.commitTransaction(client);
        return next();
      } else return httpResponseHandler.responseErrorToClient(res, 404);
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
