import {Router} from "express";
import {passportJwt} from "../../../../config/passport";
import checkUser from "../../../../cors/checkUser";
import BusinessContractController from "./CompanyBusinessContractController";

export default function (router: Router) {
  router.route('/:companyId/businesscontracts/')
    .get(passportJwt, checkUser, BusinessContractController.getBusinessContractsByCompany);

  router.route('/:companyId/businesscontracts/:businessContractId')
    .get(passportJwt, checkUser, BusinessContractController.getBusinessContractById);

  router.route('/:companyId/businesscontracts/:businessContractId/invalid')
    .post(passportJwt, checkUser, BusinessContractController.markContractAsInvalid, BusinessContractController.getBusinessContractById);

  router.route('/:companyId/businesscontracts/:businessContractId/valid')
    .post(passportJwt, checkUser, BusinessContractController.markContractAsValid, BusinessContractController.getBusinessContractById);
}
