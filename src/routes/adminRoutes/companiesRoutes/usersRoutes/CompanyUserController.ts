import {NextFunction, Request, Response} from "express";
import UserServices from "../../../../services/public/UserServices";
import {getValidateToken} from "../../../../models/IUser";
import httpResponseHandler from "../../../../cors/httpResponseHandler";
import StatusServices from "../../../../services/public/StatusServices";
import DatabaseCommand from "../../../../utils/DatabaseCommand";
import UserRoleServices from "../../../../services/public/UserRoleServices";
import RoleServices from "../../../../services/public/RoleServices";

export default {
  async getUserValidateToken(req: Request, res: Response): Promise<Response> {
    const companyId = parseInt(req.params.companyId);
    const userId = parseInt(req.params.userId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId, companyid: companyId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} and companyid ${companyId} not found.`);
      const user = users[0];
      getValidateToken(user);
      const updatedUser = await UserServices.updateOrSaveUser(user, client);
      if (!updatedUser) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 200, {
        userid: updatedUser.id,
        companyid: updatedUser.companyid,
        validatetoken: user.validatetoken
      });
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getUsers(req: Request, res: Response): Promise<Response> {
    try {
      const companyId = parseInt(req.params.companyId);
      let users = await UserServices.getUsers([{companyid: companyId}]);
      const {withroles} = req.query;
      if (typeof withroles !== 'undefined') {
        users = await Promise.all(users.map(async user => {
          const thisUser = user as any;
          thisUser.hasroles = await RoleServices.getRolesOfUser(thisUser);
          thisUser.nothaveroles = (await RoleServices.getRoles([], thisUser.companyid)).filter(role => {
            const found = thisUser.hasroles.find((item: { id: number; }) => item.id === role.id);
            return found === undefined;
          });
          return thisUser;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, users);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  async getUser(req: Request, res: Response): Promise<Response> {
    try {
      const companyId = parseInt(req.params.companyId);
      const userId = parseInt(req.params.userId);
      let users = await UserServices.getUsers([{id: userId, companyid: companyId}]);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} and companyid ${companyId} not found.`);
      const user = users[0] as any;
      user.hasroles = await RoleServices.getRolesOfUser(user);
      user.nothaveroles = (await RoleServices.getRoles([], user.companyid)).filter(role => {
        const found = user.hasroles.find((item: { id: number; }) => item.id === role.id);
        return found === undefined;
      });
      return httpResponseHandler.responseToClient(res, 200, users[0]);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  //---------------------------------------------------
  async updateUser(req: Request, res: Response, next: NextFunction) {
    const companyId = parseInt(req.params.companyId);
    const userId = parseInt(req.params.userId);
    const body = req.body;
    delete body.id;
    delete body.joinedat;
    delete body.validatetoken;
    delete body.validatetokenexpiredat;
    delete body.statusid;
    delete body.statuscode;
    delete body.companycode;
    delete body.companyid;
    const {hasroles, nothaveroles} = body;
    delete body.hasroles;
    delete body.nothaveroles;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId,
        companyid: companyId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${companyId} and companyid ${companyId} not found.`);
      let user = users[0];
      Object.assign(user, body);
      const updatedUser = await UserServices.updateOrSaveUser(user, client);
      if (!updatedUser) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await Promise.all(nothaveroles.map(async (role: { id: number; }) => {
        await UserRoleServices.removeUserRole({
          userid: updatedUser.id, companyid: updatedUser.companyid, roleid: role.id
        }, client);
      }));
      await Promise.all(hasroles.map(async (role: { id: number; }) => {
        const userRoles = await UserRoleServices.getUserRoles([{
          userid: updatedUser.id, companyid: updatedUser.companyid, roleid: role.id
        }], DatabaseCommand.lockStrength.KEY_SHARE, client);
        if (!userRoles.length) {
          await UserRoleServices.addUserRole({
            userid: updatedUser.id, companyid: updatedUser.companyid, roleid: role.id
          }, client);
        }
      }))
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  async markUserAsDisabled(req: Request, res: Response, next: NextFunction) {
    const companyId = parseInt(req.params.companyId);
    const userId = parseInt(req.params.userId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId,
        companyid: companyId,
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} and companyid ${companyId} not found.`);
      await StatusServices.setStatusCodeToObjects(UserServices.IUserTableName, 'DISABLED', [{
        id: users[0].id,
        companyid: users[0].companyid
      }], client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
  async markUserAsAvailable(req: Request, res: Response, next: NextFunction) {
    const companyId = parseInt(req.params.companyId);
    const userId = parseInt(req.params.userId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let users = await UserServices.getUsers([{
        id: userId,
        companyid: companyId,
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} and companyid ${companyId} not found.`);
      await StatusServices.setStatusCodeToObjects(UserServices.IUserTableName, 'AVAILABLE', [{
        id: users[0].id,
        companyid: users[0].companyid
      }], client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  }
}

