import {Router} from "express";
import {passportJwt} from "../../../../config/passport";
import checkUser from "../../../../cors/checkUser";
import CompanyUserController from "./CompanyUserController";
import CompanyUserRoleRoutes from "./rolesRoutes/CompanyUserRoleRoutes";

export default function (router: Router) {
  router.route("/:companyId/users")
    .get(passportJwt, checkUser, CompanyUserController.getUsers);

  router.route("/:companyId/users/:userId")
    .get(passportJwt, checkUser, CompanyUserController.getUser)
    .put(passportJwt, checkUser, CompanyUserController.updateUser, CompanyUserController.getUser);

  router.route("/:companyId/users/:userId/disabled")
    .post(passportJwt, checkUser, CompanyUserController.markUserAsDisabled, CompanyUserController.getUser);

  router.route("/:companyId/users/:userId/available")
    .post(passportJwt, checkUser, CompanyUserController.markUserAsAvailable, CompanyUserController.getUser);

  router.route("/:companyId/users/:userId/validatetoken")
    .get(passportJwt, checkUser, CompanyUserController.getUserValidateToken);

  CompanyUserRoleRoutes(router);
}
