import {Response, Request, NextFunction} from "express";
import DatabaseCommand from "../../../../../utils/DatabaseCommand";
import httpResponseHandler from "../../../../../cors/httpResponseHandler";
import UserServices from "../../../../../services/public/UserServices";
import RoleServices from "../../../../../services/public/RoleServices";
import UserRoleServices from "../../../../../services/public/UserRoleServices";

export default {
  async removeRoles(req: Request, res: Response, next: NextFunction) {
    const userId = parseInt(req.params.userId);
    const companyId = parseInt(req.params.companyId);
    const roles = req.body.roles;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const users = await UserServices.getUsers([{
        id: userId, companyId: companyId
      }], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} and companyid ${companyId} not found.`);
      await Promise.all((await RoleServices.getRoles(roles.map((role: { id: number; }) => ({
        id: role.id
      })), companyId, DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async role => {
        await UserRoleServices.removeUserRole({
          userid: userId, companyid: companyId, roleid: role.id
        }, client);
      }));
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async addRoles(req: Request, res: Response, next: NextFunction) {
    const userId = parseInt(req.params.userId);
    const companyId = parseInt(req.params.companyId);
    const roles = req.body.roles;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const users = await UserServices.getUsers([{
        id: userId, companyId: companyId
      }], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!users.length) return httpResponseHandler.responseErrorToClient(res, 404, `User has id ${userId} and companyid ${companyId} not found.`);
      await Promise.all((await RoleServices.getRoles(roles.map((role: { id: number; }) => ({
        id: role.id
      })), companyId, DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async role => {
        const userRoles = await UserRoleServices.getUserRoles([{
          userid: userId, companyid: companyId, roleid: role.id
        }], DatabaseCommand.lockStrength.KEY_SHARE, client);
        if (!userRoles.length) await UserRoleServices.addUserRole({
          userid: userId, companyid: companyId, roleid: role.id
        }, client);
      }));
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
