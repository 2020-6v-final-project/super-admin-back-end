import {Router} from "express";
import {passportJwt} from "../../../../../config/passport";
import checkUser from "../../../../../cors/checkUser";
import CompanyUserRoleController from "./CompanyUserRoleController";
import CompanyUserController from "../CompanyUserController";

export default function (router: Router) {
  router.route("/:companyId/users/:userId/roles")
    .post(passportJwt, checkUser, CompanyUserRoleController.addRoles, CompanyUserController.getUser)
    .delete(passportJwt, checkUser, CompanyUserRoleController.removeRoles, CompanyUserController.getUser);
}
