import {Response, Request, NextFunction} from "express";
import ContractTypeServices from "../../../services/public/ContractTypeServices";
import RoleServices from "../../../services/public/RoleServices";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import PermissionServices from "../../../services/public/PermissionServices";
import ContractTypePermissionServices from "../../../services/public/ContractTypePermissionServices";

export default {

  async getBusinessContractTypes(req: Request, res: Response): Promise<Response> {
    try {
      let types = await ContractTypeServices.getContractTypes([]);
      const {withpermissions} = req.query;
      if (typeof withpermissions !== 'undefined') {
        types = await Promise.all(types.map(async item => {
          const type = item as any;
          type.haspermissions = await PermissionServices.getPermissionsOfTypeId(type.id);
          type.nothavepermissions = (await PermissionServices.getPermissions([])).filter(permission => {
            const found = type.haspermissions.find((item: { id: number; }) => item.id === permission.id);
            return found === undefined;
          });
          return type;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, types);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createBusinessContractTypes(req: Request, res: Response): Promise<Response> {
    const body = req.body;
    delete body.id;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const result = await ContractTypeServices.createContractType(body, client);
      if (!result) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 201, result);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getBusinessContractType(req: Request, res: Response) {
    const contractTypeId = parseInt(req.params.contractTypeId);
    try {
      const contractTypes = await ContractTypeServices.getContractTypes([{
        id: contractTypeId
      }]);
      if (!contractTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Contract type has id ${contractTypeId} not found.`);
      const type = contractTypes[0] as any;
      type.haspermissions = await PermissionServices.getPermissionsOfTypeId(type.id);
      type.nothavepermissions = (await PermissionServices.getPermissions([])).filter(permission => {
        const found = type.haspermissions.find((item: { id: number; }) => item.id === permission.id);
        return found === undefined;
      });
      return httpResponseHandler.responseToClient(res, 200, type);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async updateBusinessContractType(req: Request, res: Response, next: NextFunction) {
    const contractTypeId = parseInt(req.params.contractTypeId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const contractTypes = await ContractTypeServices.getContractTypes([{
        id: contractTypeId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!contractTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Contract type has id ${contractTypeId} not found.`);
      const contractType = contractTypes[0];
      const body = req.body;
      const { haspermissions, nothavepermissions } = body;
      delete body.haspermissions;
      delete body.nothavepermissions;
      delete body.id;
      Object.assign(contractType, body);
      const updatedContractType = await ContractTypeServices.updateContractType(contractType, client);
      if (!updatedContractType) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      if (nothavepermissions instanceof Array) {
        await Promise.all(nothavepermissions.map(async permission => {
          await ContractTypePermissionServices.deleteTypePermissions([{
            contracttypeid: updatedContractType.id, permissionid: permission.id
          }], client);
        }));
      }
      if (haspermissions instanceof Array) {
        await Promise.all(haspermissions.map(async permission => {
          const contractTypePermissions = await ContractTypePermissionServices.getTypePermissions([{
            contracttypeid: updatedContractType.id, permissionid: permission.id
          }], DatabaseCommand.lockStrength.KEY_SHARE, client);
          if (!contractTypePermissions.length) {
            await ContractTypePermissionServices.createTypePermission({
              contracttypeid: updatedContractType.id, permissionid: permission.id
            }, client);
          }
        }));
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async deleteBusinessContractType(req: Request, res: Response) {
    const contractTypeId = parseInt(req.params.contractTypeId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const contractTypes = await ContractTypeServices.getContractTypes([{
        id: contractTypeId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      if (!contractTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Contract type has id ${contractTypeId} not found.`);
      const contractType = contractTypes[0];
      await ContractTypePermissionServices.deleteTypePermissions([{
        contracttypeid: contractType.id
      }], client);
      const deletedType = await ContractTypeServices.deleteContractType(contractType, client);
      if (!deletedType) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  }
}
