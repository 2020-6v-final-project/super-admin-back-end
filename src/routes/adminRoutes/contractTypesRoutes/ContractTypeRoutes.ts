import {Application, Router} from "express";
import Routes from "../../../constant/Routes";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";
import ContractTypeController from "./ContractTypeController";

const router = Router();

export default function(app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, ContractTypeController.getBusinessContractTypes)
    .post(passportJwt, checkUser, ContractTypeController.createBusinessContractTypes)

  router.route('/:contractTypeId')
    .get(passportJwt, checkUser, ContractTypeController.getBusinessContractType)
    .put(passportJwt, checkUser, ContractTypeController.updateBusinessContractType, ContractTypeController.getBusinessContractType)
    .delete(passportJwt, checkUser, ContractTypeController.deleteBusinessContractType);

  app.use(Routes.ADMIN_CONTRACT_TYPES, router);
}
