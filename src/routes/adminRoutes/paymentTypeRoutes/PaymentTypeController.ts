import {NextFunction, Request, Response} from 'express';
import httpResponseHandler from "../../../cors/httpResponseHandler";
import PaymentTypeServices from "../../../services/public/PaymentTypeServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";

export default {

  async getPaymentTypes(req: Request, res: Response) {
    try {
      const paymentTypes = await PaymentTypeServices.getPaymentTypes([]);
      return httpResponseHandler.responseToClient(res, 200, paymentTypes);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async addPaymentType(req: Request, res: Response) {
    const client = await DatabaseCommand.newClient();
    const body = req.body;
    delete body.id;
    try {
      await DatabaseCommand.beginTransaction(client);
      const insertedType = await PaymentTypeServices.createPaymentType(body, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 201, insertedType);
    } catch (e) {
      await DatabaseCommand.rollbackTransaction(client);
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async updatePaymentType(req: Request, res: Response, next: NextFunction) {
    const client = await DatabaseCommand.newClient();
    const body = req.body;
    const paymentTypeId = parseInt(req.params.paymentTypeId);
    delete body.id;
    try {
      await DatabaseCommand.beginTransaction(client);
      const paymentTypes = await PaymentTypeServices.getPaymentTypes([{
        id: paymentTypeId
      }], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!paymentTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment type has id ${paymentTypeId} not found.`);
      const updatedPayments = await PaymentTypeServices.updatePaymentTypes([{
        id: paymentTypeId
      }], body, client);
      if (!updatedPayments.length) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      await DatabaseCommand.rollbackTransaction(client);
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async deletePaymentType(req: Request, res: Response) {
    const client = await DatabaseCommand.newClient();
    const paymentTypeId = parseInt(req.params.paymentTypeId);
    try {
      await DatabaseCommand.beginTransaction(client);
      const paymentTypes = await PaymentTypeServices.getPaymentTypes([{
        id: paymentTypeId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      if (!paymentTypes.length) return httpResponseHandler.responseErrorToClient(res, 404, `Payment type has id ${paymentTypeId} not found.`);
      const deletedTypes = await PaymentTypeServices.deletePaymentTypes([{
        id: paymentTypeId
      }], client);
      if (!deletedTypes.length) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      await DatabaseCommand.rollbackTransaction(client);
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
