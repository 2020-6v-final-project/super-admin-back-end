import {Application, Router} from "express";
import Routes from "../../../constant/Routes";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";
import PaymentTypeController from "./PaymentTypeController";

const router = Router();

export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, PaymentTypeController.getPaymentTypes)
    .post(passportJwt, checkUser, PaymentTypeController.addPaymentType);

  router.route('/:paymentTypeId')
    .put(passportJwt, checkUser, PaymentTypeController.updatePaymentType)
    .delete(passportJwt, checkUser, PaymentTypeController.deletePaymentType);

  app.use(Routes.ADMIN_PAYMENT_TYPES, router);
}
