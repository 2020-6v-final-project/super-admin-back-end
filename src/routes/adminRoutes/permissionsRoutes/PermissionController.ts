import {NextFunction, Request, Response} from "express";
import ApiServices from "../../../services/public/ApiServices";
import httpResponseHandler from "../../../cors/httpResponseHandler";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import PermissionApiServices from "../../../services/public/PermissionApiServices";
import PermissionServices from "../../../services/public/PermissionServices";

export default {
  async getPermissions(req: Request, res: Response): Promise<Response> {
    try {
      let permissions = await PermissionServices.getPermissions([]);
      const { withapis } = req.query;
      if (typeof withapis !== 'undefined') {
        permissions = await Promise.all(permissions.map(async item => {
          const permission = item as any;
          permission.hasapis = await Promise.all((await PermissionApiServices.getPermissionApis([{
            permissionid: permission.id
          }])).map(async permissionApi => {
            const rows = await ApiServices.getApis([{id: permissionApi.apiid}]);
            return rows[0];
          }));
          permission.nothaveapis = (await ApiServices.getApis([])).filter(api => {
            for (const iApi of permission.hasapis) {
              if (iApi.id === api.id) {
                return false;
              }
            }
            return true;
          });
          return permission;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, permissions);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async deletePermission(req: Request, res: Response): Promise<Response> {
    const permissionId = parseInt(req.params.permissionId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const permissions = await PermissionServices.getPermissions([{id: permissionId}], DatabaseCommand.lockStrength.UPDATE, client);
      if (!permissions.length) return httpResponseHandler.responseErrorToClient(res, 404, `Permission has id ${permissionId} not found.`);
      const result = await PermissionServices.deletePermission(permissions[0], client);
      if (!result) return httpResponseHandler.responseErrorToClient(res, 500);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async addPermission(req: Request, res: Response): Promise<Response> {
    const permission = req.body;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const createdPermission = await PermissionServices.createPermission(permission, client);
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 201, createdPermission);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getPermission(req: Request, res: Response): Promise<Response> {
    const permissionId = parseInt(req.params.permissionId);
    try {
      const permissions = await PermissionServices.getPermissions([{id: permissionId}]);
      if (!permissions.length) return httpResponseHandler.responseErrorToClient(res, 404, `Permission has id ${permissionId} not found.`);
      const permission = permissions[0] as any;
      permission.hasapis = await Promise.all((await PermissionApiServices.getPermissionApis([{
        permissionid: permissionId
      }])).map(async permissionApi => {
        const rows = await ApiServices.getApis([{id: permissionApi.apiid}]);
        return rows[0];
      }));
      permission.nothaveapis = (await ApiServices.getApis([])).filter(api => {
        for (const iApi of permission.hasapis) {
          if (iApi.id === api.id) {
            return false;
          }
        }
        return true;
      });
      return httpResponseHandler.responseToClient(res, 200, permission);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async updatePermission(req: Request, res: Response, next: NextFunction) {
    const permissionId = parseInt(req.params.permissionId);
    const permission = req.body;
    delete permission.id;
    const {hasapis, nothaveapis} = permission;
    delete permission.hasapis;
    delete permission.nothaveapis;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const permissions = await PermissionServices.getPermissions([{id: permissionId}], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!permissions.length) return httpResponseHandler.responseErrorToClient(res, 404, `Permission has id ${permissionId} not found.`);
      const newPermission = permissions[0];
      Object.assign(newPermission, permission);
      const updatedPermission = await PermissionServices.updatePermission(newPermission, client);
      if (!updatedPermission) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      await Promise.all((await ApiServices.getApis(nothaveapis.map((api: { id: number; }) => ({
        id: api.id
      })), DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async api => {
        await PermissionApiServices.deletePermissionApi({
          permissionid: permissionId, apiid: api.id
        }, client);
      }));
      await Promise.all((await ApiServices.getApis(hasapis.map((api: { id: number; }) => ({
        id: api.id
      })), DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async api => {
        const permissionApis = await PermissionApiServices.getPermissionApis([{
          permissionid: permissionId, apiid: api.id
        }], DatabaseCommand.lockStrength.KEY_SHARE, client);
        if (!permissionApis.length) await PermissionApiServices.createPermissionApi({
          permissionid: permissionId, apiid: api.id
        }, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
