import {Router, Application} from 'express';
import Routes from "../../../constant/Routes";
import {passportJwt} from "../../../config/passport";
import checkUser from "../../../cors/checkUser";
import PermissionController from "./PermissionController";
import PermissionApiRoutes from "./apiRoutes/PermissionApiRoutes";

const router = Router();

export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, PermissionController.getPermissions)
    .post(passportJwt, checkUser, PermissionController.addPermission);

  router.route('/:permissionId')
    .get(passportJwt, checkUser, PermissionController.getPermission)
    .put(passportJwt, checkUser, PermissionController.updatePermission, PermissionController.getPermission)
    .delete(passportJwt, checkUser, PermissionController.deletePermission);

  PermissionApiRoutes(router);

  app.use(Routes.ADMIN_PERMISSIONS, router);
}
