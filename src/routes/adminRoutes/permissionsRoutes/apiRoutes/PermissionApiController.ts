import {NextFunction, Request, Response} from "express";
import DatabaseCommand from "../../../../utils/DatabaseCommand";
import PermissionApiServices from "../../../../services/public/PermissionApiServices";
import httpResponseHandler from "../../../../cors/httpResponseHandler";
import ApiServices from "../../../../services/public/ApiServices";
import PermissionServices from "../../../../services/public/PermissionServices";

export default {
  async addApis(req: Request, res: Response, next: NextFunction) {
    const permissionId = parseInt(req.params.permissionId);
    let apis = req.body.apiids;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const permissions = await PermissionServices.getPermissions([{id: permissionId}], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!permissions.length) return httpResponseHandler.responseErrorToClient(res, 404, `Permission has id ${permissionId} not found.`);
      await Promise.all((await ApiServices.getApis(apis.map((api: { id: number; }) => ({
        id: api.id
      })), DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async api => {
        const permissionApis = await PermissionApiServices.getPermissionApis([{
          permissionid: permissionId, apiid: api.id
        }], DatabaseCommand.lockStrength.KEY_SHARE, client);
        if (!permissionApis.length) await PermissionApiServices.createPermissionApi({
          permissionid: permissionId, apiid: api.id
        }, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async deleteApis(req: Request, res: Response, next: NextFunction) {
    const permissionId = parseInt(req.params.permissionId);
    let apis = req.body.apiids
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const permissions = await PermissionServices.getPermissions([{id: permissionId}], DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!permissions.length) return httpResponseHandler.responseErrorToClient(res, 404, `Permission has id ${permissionId} not found.`);
      await Promise.all((await ApiServices.getApis(apis.map((api: { id: number; }) => ({
        id: api.id
      })), DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async api => {
        await PermissionApiServices.deletePermissionApi({
          permissionid: permissionId, apiid: api.id
        }, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
