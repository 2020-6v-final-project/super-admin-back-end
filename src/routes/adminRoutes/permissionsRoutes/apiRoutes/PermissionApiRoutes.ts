import {Router} from "express";
import {passportJwt} from "../../../../config/passport";
import checkUser from "../../../../cors/checkUser";
import PermissionApiController from "./PermissionApiController";
import PermissionController from "../PermissionController";

export default function (router: Router) {
  router.route('/:permissionId/apis')
    .post(passportJwt, checkUser, PermissionApiController.addApis, PermissionController.getPermission)
    .delete(passportJwt, checkUser, PermissionApiController.deleteApis, PermissionController.getPermission);
}
