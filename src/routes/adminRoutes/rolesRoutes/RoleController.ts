import {NextFunction, Request, Response} from "express";
import httpResponseHandler from '../../../cors/httpResponseHandler';
import RoleServices from "../../../services/public/RoleServices";
import UserRoleServices from "../../../services/public/UserRoleServices";
import UserServices from "../../../services/public/UserServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import PermissionServices from "../../../services/public/PermissionServices";
import CompanyRoleServices from "../../../services/public/CompanyRoleServices";
import RolePermissionServices from "../../../services/public/RolePermissionServices";
import { IPermission } from "../../../models/IPermission";
import { IRolePermission } from "../../../models/IRolePermission";

export default {
  async getRoles(req: Request, res: Response) {
    try {
      let roles = await RoleServices.getRoles([]);
      const {withpermissions} = req.query;
      if (typeof withpermissions !== 'undefined') {
        roles = await Promise.all(roles.map(async item => {
          const role = item as any;
          role.haspermissions = await PermissionServices.getPermissionsOfRoleIds([role.id]);
          role.nothavepermissions = (await PermissionServices.getPermissions([])).filter(permission => {
            const found = role.haspermissions.find((item: { id: number; }) => item.id === permission.id);
            return found === undefined;
          });
          return role;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, roles)
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async getUsersOfRole(req: Request, res: Response): Promise<Response> {
    try {
      const roleId = parseInt(req.params.roleId);
      const roles = await RoleServices.getRoles([{id: roleId}]);
      if (!roles) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const userRoles = await UserRoleServices.getUserRoles([{roleid: roleId}]);
      let users = await Promise.all(userRoles.map(async element => {
        return await UserServices.getUsers([{id: element.userid, companyid: element.companyid}]);
      }));
      const {withroles} = req.query;
      if (typeof withroles !== 'undefined') {
        users = await Promise.all(users.map(async user => {
          const thisUser = user as any;
          thisUser.hasroles = await RoleServices.getRolesOfUser(thisUser);
          thisUser.nothaveroles = (await RoleServices.getRoles([], thisUser.companyid)).filter(role => {
            const found = thisUser.hasroles.find((item: { id: number; }) => item.id === role.id);
            return found === undefined;
          });
          return thisUser;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, users);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async createRole(req: Request, res: Response, next: NextFunction) {
    const bodyRole = req.body;
    const companyId = bodyRole.companyid;
    delete bodyRole.id;
    delete bodyRole.isprimary;
    delete bodyRole.companyid;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const createdRole = await RoleServices.createRole(bodyRole, client);
      if (companyId) {
        await CompanyRoleServices.createCompanyRole({
          companyid: companyId, roleid: createdRole.id
        });
      }
      await DatabaseCommand.commitTransaction(client);
      req.params.roleId = createdRole.id.toString();
      next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async getRole(req: Request, res: Response) {
    const roleId = parseInt(req.params.roleId);
    try {
      const roles = await RoleServices.getRoles([{id: roleId}]);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const role = roles[0] as any;
      role.haspermissions = await PermissionServices.getPermissionsOfRoleIds([role.id]);
      role.nothavepermissions = (await PermissionServices.getPermissions([])).filter(permission => {
        const found = role.haspermissions.find((item: { id: number; }) => item.id === permission.id);
        return found === undefined;
      });
      return httpResponseHandler.responseToClient(res, 200, role);
    } catch (e) {
      console.log(e);
      return httpResponseHandler.responseErrorToClient(res, 500);
    }
  },
  async deleteRole(req: Request, res: Response): Promise<Response> {
    const roleId = parseInt(req.params.roleId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{
        id: roleId,
        isprimary: false
      }], undefined, DatabaseCommand.lockStrength.UPDATE, client);
      if (!roles.length) {
        return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found or can not delete.`);
      }
      const rolePermissions = await RolePermissionServices.getRolePermissions([{
        roleid: roleId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      await Promise.all(rolePermissions.map(async rolePermission => {
        return await RolePermissionServices.deleteRolePermission(rolePermission, client);
      }));
      const companyRoles = await CompanyRoleServices.getCompanyRoles([{
        roleid: roleId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      await Promise.all(companyRoles.map(async companyRole => {
        return await CompanyRoleServices.deleteCompanyRole(companyRole, client);
      }));
      const userRoles = await UserRoleServices.getUserRoles([{
        roleid: roleId
      }], DatabaseCommand.lockStrength.UPDATE, client);
      await Promise.all(userRoles.map(async userRole => {
        return await UserRoleServices.removeUserRole(userRole, client);
      }));
      let result = await RoleServices.deleteRole(roles[0], client);
      if (!result) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 500);
      }
      await DatabaseCommand.commitTransaction(client);
      return httpResponseHandler.responseToClient(res, 202);
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async updateRole(req: Request, res: Response, next: NextFunction) {
    const roleId = parseInt(req.params.roleId);
    const bodyRole = req.body;
    delete bodyRole.id;
    delete bodyRole.code;
    const haspermissions = bodyRole.haspermissions as IPermission[];
    delete bodyRole.haspermissions;
    delete bodyRole.nothavepermissions;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{id: roleId}], undefined, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const newRole = roles[0];
      Object.assign(newRole, bodyRole);
      let updatedRole = await RoleServices.updateRole(newRole, client);
      if (!updatedRole) {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
      const oldRolePermission = await RolePermissionServices.getRolePermissions([{roleid: roleId}], DatabaseCommand.lockStrength.KEY_SHARE, client);
      for(let rolepermission of oldRolePermission) {
        const containIndex = haspermissions.findIndex(permission => permission.id === rolepermission.permissionid);
        if (containIndex > -1) {
          haspermissions.splice(containIndex, 1);
        } else await RolePermissionServices.deleteRolePermission(rolepermission, client);
      }
      await Promise.all(haspermissions.map(async permission => {
        const newRolePermission: IRolePermission = {
          roleid: roleId,
          permissionid: permission.id
        }
        await RolePermissionServices.createRolePermission(newRolePermission, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async markRoleAsPrimary(req: Request, res: Response, next: NextFunction) {
    const roleId = parseInt(req.params.roleId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{
        id: roleId
      }], undefined, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const role = roles[0];
      role.isprimary = true;
      await RoleServices.updateRole(role, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  },
  async markRoleAsNotPrimary(req: Request, res: Response, next: NextFunction) {
    const roleId = parseInt(req.params.roleId);
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{
        id: roleId
      }], undefined, DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      const role = roles[0];
      role.isprimary = false;
      await RoleServices.updateRole(role, client);
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
};
