import {Router, Application} from 'express';
import Routes from "../../../constant/Routes";
import RoleController from "./RoleController";
import checkUser from "../../../cors/checkUser";
import {passportJwt} from "../../../config/passport";
import RolePermissionRoutes from "./permissionsRoutes/RolePermissionRoutes";

const router = Router();

export default function (app: Application) {
  router.route('/')
    .get(passportJwt, checkUser, RoleController.getRoles)
    .post(passportJwt, checkUser, RoleController.createRole, RoleController.getRole);

  router.route('/:roleId')
    .get(passportJwt, checkUser, RoleController.getRole)
    .put(passportJwt, checkUser, RoleController.updateRole, RoleController.getRole)
    .delete(passportJwt, checkUser, RoleController.deleteRole);

  RolePermissionRoutes(router);

  router.route('/:roleId/primary')
    .post(passportJwt, checkUser, RoleController.markRoleAsPrimary, RoleController.getRole)
    .delete(passportJwt, checkUser, RoleController.markRoleAsNotPrimary, RoleController.getRole);

  router.route('/:roleId/users')
    .get(passportJwt, checkUser, RoleController.getUsersOfRole);
    // .put(passportJwt, checkUser, RoleController.addUserToRole)

  app.use(Routes.ADMIN_ROLES, router);
}
