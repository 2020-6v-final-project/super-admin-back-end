import {NextFunction, Request, Response} from "express";
import DatabaseCommand from "../../../../utils/DatabaseCommand";
import httpResponseHandler from "../../../../cors/httpResponseHandler";
import RolePermissionServices from "../../../../services/public/RolePermissionServices";
import RoleServices from "../../../../services/public/RoleServices";
import PermissionServices from "../../../../services/public/PermissionServices";

export default {
  async addPermissions(req: Request, res: Response, next: NextFunction) {
    const roleId = parseInt(req.params.roleId);
    let permissions = req.body.permissions;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{id: roleId}], undefined, DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      await Promise.all((await PermissionServices.getPermissions(permissions.map((permission: { id: number; }) => ({
        id: permission.id
      })), DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async (permission: { id: number; }) => {
        const rolePermissions = await RolePermissionServices.getRolePermissions([{
          permissionid: permission.id, roleid: roleId
        }]);
        if (!rolePermissions.length) await RolePermissionServices.createRolePermission({
          permissionid: permission.id, roleid: roleId
        });
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return next();
    } finally {
      client.release();
    }
  },
  async deletePermissions(req: Request, res: Response, next: NextFunction) {
    const roleId = parseInt(req.params.roleId);
    let permissions = req.body.permissions;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      const roles = await RoleServices.getRoles([{id: roleId}], undefined, DatabaseCommand.lockStrength.KEY_SHARE, client);
      if (!roles.length) return httpResponseHandler.responseErrorToClient(res, 404, `Role has id ${roleId} not found.`);
      await Promise.all((await PermissionServices.getPermissions(permissions.map((permission: { id: number; }) => ({
        id: permission.id
      })), DatabaseCommand.lockStrength.KEY_SHARE, client)).map(async (permission: { id: number; }) => {
        await RolePermissionServices.deleteRolePermission({
          permissionid: permission.id, roleid: roleId
        }, client);
      }));
      await DatabaseCommand.commitTransaction(client);
      return next();
    } catch (e) {
      console.log(e);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 500);
    } finally {
      client.release();
    }
  }
}
