import {Router} from "express";
import {passportJwt} from "../../../../config/passport";
import checkUser from "../../../../cors/checkUser";
import RolePermissionController from "./RolePermissionController";
import RoleController from "../RoleController";

export default function (router: Router) {
  router.route('/:roleId/permissions')
    .post(passportJwt, checkUser, RolePermissionController.addPermissions, RoleController.getRole)
    .delete(passportJwt, checkUser, RolePermissionController.deletePermissions, RoleController.getRole);
}
