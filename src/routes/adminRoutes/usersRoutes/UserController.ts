import {Request, Response} from "express";
import UserServices from "../../../services/public/UserServices";
import httpResponseHandler from '../../../cors/httpResponseHandler';
import CompanyServices from "../../../services/public/CompanyServices";
import DatabaseCommand from "../../../utils/DatabaseCommand";
import AccountServices from "../../../services/public/AccountServices";
import {setPassword} from "../../../models/IAccount";
import Functions from "../../../utils/Functions";

export default {
  getAllUser: async (req: Request, res: Response): Promise<Response> => {
    try {
      let users = await UserServices.getUsers([]);
      const {withcompany} = req.query;
      if (withcompany) {
        users = await Promise.all(users.map(async user => {
          const tempUser = user as any;
          delete tempUser.companyid;
          tempUser.company = await CompanyServices.getCompanies([{id: user.companyid}]);
          return tempUser;
        }));
      }
      return httpResponseHandler.responseToClient(res, 200, users);
    } catch (err) {
      console.log(err);
      return httpResponseHandler.responseErrorToClient(res, 500, err.message);
    }
  },
  async addUser(req: Request, res: Response): Promise<Response> {
    const body = req.body;
    delete body.id;
    delete body.joinedat;
    delete body.validatetoken;
    delete body.validatetokenexpiredat;
    delete body.statusid;
    delete body.statuscode;
    const client = await DatabaseCommand.newClient();
    try {
      await DatabaseCommand.beginTransaction(client);
      let companies = await CompanyServices.getCompanies([{id: body.companyid}], DatabaseCommand.lockStrength.NO_KEY_UPDATE, client);
      if (!companies.length) return httpResponseHandler.responseErrorToClient(res, 404, `Company has id ${body.companyid} not found.`);
      let userCompany = companies[0];
      body.companyid = userCompany.id;
      body.companycode = userCompany.code;
      userCompany.numberofusers++;
      const updatedCompany = await CompanyServices.updateCompany(userCompany, undefined, client);
      if (updatedCompany) {
        let {newUser, newAccount} = await UserServices.createUserWithAccount(body, client);
        await DatabaseCommand.commitTransaction(client);
        return httpResponseHandler.responseToClient(res, 201, {
          newUser,
          newAccount
        });
      } else {
        await DatabaseCommand.rollbackTransaction(client);
        return httpResponseHandler.responseErrorToClient(res, 400);
      }
    } catch (err) {
      console.log(err);
      await DatabaseCommand.rollbackTransaction(client);
      return httpResponseHandler.responseErrorToClient(res, 400, err.message);
    } finally {
      client.release();
    }
  },
};
