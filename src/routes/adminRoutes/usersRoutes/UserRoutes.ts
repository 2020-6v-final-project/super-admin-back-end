import { Router, Application } from 'express';
import Routes from '../../../constant/Routes';
import AdminUserController from "./UserController";
import checkUser from "../../../cors/checkUser";
import {passportJwt} from "../../../config/passport";

const router: Router = Router();

export default (app: Application) => {

  router.route('/')
    .get(passportJwt, checkUser, AdminUserController.getAllUser)
    .post(passportJwt, checkUser, AdminUserController.addUser);

  app.use(Routes.ADMIN_USERS, router)
};
