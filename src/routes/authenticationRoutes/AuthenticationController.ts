import {validationResult} from "express-validator";
import {Request, Response} from "express";
import {IAccount, setPassword} from "../../models/IAccount";
import AccountServices from "../../services/public/AccountServices";
import UserServices from "../../services/public/UserServices";
import httpResponseHandler from "../../cors/httpResponseHandler";
import DatabaseCommand from "../../utils/DatabaseCommand";

export default {
  /**
   * Create account and return the jwt
   * @param req
   * @param res
   */
  // register: async (req: Request, res: Response) => {
  //   const client = await DatabaseCommand.newClient();
  //   try {
  //     const {userId, validatetoken, username, password} = req.body;
  //     const account = await AccountServices.getAccountByUsernameAndCompanyCode(username, undefined, client);
  //     if (account) return httpResponseHandler.responseErrorToClient(res, 409, "Account already exists.");
  //     const user = await UserServices.getUserById(userId, DatabaseCommand.lockStrength.SHARE, client);
  //     if (!user) return httpResponseHandler.responseErrorToClient(res, 404, "User not found.");
  //     if (!(user.statuscode === 'AVAILABLE')) return httpResponseHandler.responseErrorToClient(res, 403, 'User not available.');
  //     const thisDate = new Date();
  //     if (user.validatetoken !== validatetoken || user.validatetokenexpiredat < thisDate) return httpResponseHandler.responseErrorToClient(res, 403, "This validate token is expired.");
  //     let newAccount = {
  //       userid: userId,
  //       username: username,
  //       hash: '',
  //       salt: ''
  //     };
  //     setPassword(password, newAccount);
  //     newAccount = await AccountServices.createAccount(newAccount, client);
  //     await DatabaseCommand.commitTransaction(client);
  //     httpResponseHandler.responseToClient(res, 201, {newAccount});
  //   } catch(err) {
  //     console.log(err);
  //     await DatabaseCommand.rollbackTransaction(client);
  //     httpResponseHandler.responseErrorToClient(res, 500);
  //   } finally {
  //     client.release();
  //   }
  // },
};
