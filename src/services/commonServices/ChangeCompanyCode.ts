export function getAlterScript(companyId: number, oldCode: string, newCode: string): string {
  return `
SELECT * FROM public.users WHERE companyid = ${companyId} FOR UPDATE;
UPDATE public.users SET companycode = '${newCode}' WHERE companyid = ${companyId};
SELECT * FROM public.accounts WHERE companyid = ${companyId} FOR UPDATE;
UPDATE public.accounts SET username = CONCAT(SPLIT_PART(username, '@', 1),'@','${newCode.toLowerCase()}') WHERE companyid = ${companyId};
SELECT * FROM ${oldCode}.branches FOR UPDATE;
UPDATE ${oldCode}.branches SET code = CONCAT('${newCode}', '-', SPLIT_PART(code, '-', 2));
ALTER SCHEMA ${oldCode} RENAME TO ${newCode.toLowerCase()};
  `
}
