export function getCreateScript(companyCode: string): string {
  companyCode = companyCode.toLowerCase();
  return `--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-03-16 16:29:38

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 17164)
-- Name: ${companyCode}; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA ${companyCode};


--
-- TOC entry 201 (class 1259 OID 17165)
-- Name: branches; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.branches (
    id integer NOT NULL,
    code character varying(30),
    name character varying(50),
    numberofusers integer DEFAULT 0,
    address character varying(100),
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    mainmenu integer,
    manager integer,
    taxcalculated boolean DEFAULT true,
    currencyid integer DEFAULT 1 NOT NULL
);


--
-- TOC entry 202 (class 1259 OID 17173)
-- Name: branches_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.branches_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 202
-- Name: branches_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.branches_id_seq OWNED BY ${companyCode}.branches.id;


--
-- TOC entry 203 (class 1259 OID 17175)
-- Name: branches_items; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.branches_items (
    branchid integer NOT NULL,
    itemid integer NOT NULL,
    currentquantity integer DEFAULT 0,
    statusid integer DEFAULT 9,
    statuscode character varying(30) DEFAULT 'OUTOFORDER'::character varying
);


--
-- TOC entry 204 (class 1259 OID 17181)
-- Name: checks; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.checks (
    id bigint NOT NULL,
    accountid integer NOT NULL,
    oldbalance numeric(14,2),
    debitamount numeric(12,2) NOT NULL,
    fee numeric(12,2) DEFAULT 0,
    newbalance numeric(14,2),
    note text,
    createdat timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    createdby integer NOT NULL,
    receiptid bigint,
    approvedby integer,
    approvedat timestamp with time zone
);


--
-- TOC entry 205 (class 1259 OID 17189)
-- Name: checks_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.checks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 205
-- Name: checks_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.checks_id_seq OWNED BY ${companyCode}.checks.id;


--
-- TOC entry 206 (class 1259 OID 17191)
-- Name: deposits; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.deposits (
    id bigint NOT NULL,
    accountid integer NOT NULL,
    creditamount numeric(12,2) NOT NULL,
    fee numeric(12,2) DEFAULT 0,
    statusid integer DEFAULT 12,
    receivedat timestamp with time zone,
    oldbalance numeric(14,2),
    newbalance numeric(14,2),
    createdby integer,
    orderid bigint,
    note text,
    checkid bigint,
    statuscode character varying(30) DEFAULT 'PENDING'::character varying
);


--
-- TOC entry 207 (class 1259 OID 17200)
-- Name: deposits_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.deposits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 207
-- Name: deposits_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.deposits_id_seq OWNED BY ${companyCode}.deposits.id;


--
-- TOC entry 208 (class 1259 OID 17202)
-- Name: financeaccounts_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.financeaccounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 209 (class 1259 OID 17204)
-- Name: financeaccounts; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.financeaccounts (
    id integer DEFAULT nextval('${companyCode}.financeaccounts_id_seq'::regclass) NOT NULL,
    currentbalance numeric(14,2) DEFAULT 0,
    branchid integer,
    accounttype character varying(30),
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    currencyid integer DEFAULT 1
);


--
-- TOC entry 210 (class 1259 OID 17212)
-- Name: financevalidates; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.financevalidates (
    id bigint NOT NULL,
    accountid integer,
    currentbalance numeric(14,2),
    actualbalance numeric(14,2),
    difference numeric(12,2),
    note text,
    validatedat timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    validatedby integer
);


--
-- TOC entry 211 (class 1259 OID 17219)
-- Name: ingredients; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.ingredients (
    id integer NOT NULL,
    name character varying(50),
    unit character varying(30),
    baseprice numeric(12,2),
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    imgpath text,
    price numeric(12,2),
    typeid integer,
    lastmodified timestamp with time zone
);


--
-- TOC entry 212 (class 1259 OID 17227)
-- Name: ingredients_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.ingredients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 212
-- Name: ingredients_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.ingredients_id_seq OWNED BY ${companyCode}.ingredients.id;


--
-- TOC entry 213 (class 1259 OID 17229)
-- Name: ingredienttypes; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.ingredienttypes (
    id integer NOT NULL,
    name character varying(30)
);


--
-- TOC entry 214 (class 1259 OID 17232)
-- Name: ingredienttypes_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.ingredienttypes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 214
-- Name: ingredienttypes_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.ingredienttypes_id_seq OWNED BY ${companyCode}.ingredienttypes.id;


--
-- TOC entry 215 (class 1259 OID 17234)
-- Name: items; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.items (
    id integer NOT NULL,
    name character varying(50),
    unit character varying(30),
    price numeric(12,2),
    lastmodified timestamp with time zone,
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    recipe text,
    description text,
    imgpath text,
    baseprice numeric(12,2),
    typeid integer
);


--
-- TOC entry 216 (class 1259 OID 17242)
-- Name: items_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 216
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.items_id_seq OWNED BY ${companyCode}.items.id;


--
-- TOC entry 217 (class 1259 OID 17244)
-- Name: items_ingredients; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.items_ingredients (
    itemid integer NOT NULL,
    ingredientid integer NOT NULL,
    quantity double precision
);


--
-- TOC entry 218 (class 1259 OID 17247)
-- Name: items_options; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.items_options (
    itemid integer NOT NULL,
    optionid integer NOT NULL,
    note text,
    price numeric(12,2) NOT NULL
);


--
-- TOC entry 219 (class 1259 OID 17253)
-- Name: items_options_ingredients; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.items_options_ingredients (
    itemid integer NOT NULL,
    optionid integer NOT NULL,
    ingredientid integer NOT NULL,
    ingredientquantity double precision
);


--
-- TOC entry 220 (class 1259 OID 17256)
-- Name: itemtypes; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.itemtypes (
    id integer NOT NULL,
    name character varying(30)
);


--
-- TOC entry 221 (class 1259 OID 17259)
-- Name: itemtypes_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.itemtypes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 221
-- Name: itemtypes_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.itemtypes_id_seq OWNED BY ${companyCode}.itemtypes.id;


--
-- TOC entry 222 (class 1259 OID 17261)
-- Name: menus; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.menus (
    id integer NOT NULL,
    name character varying(50),
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    imgpath text,
    description text,
    lastmodified timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);


--
-- TOC entry 223 (class 1259 OID 17270)
-- Name: menus_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.menus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 223
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.menus_id_seq OWNED BY ${companyCode}.menus.id;


--
-- TOC entry 224 (class 1259 OID 17272)
-- Name: menus_items; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.menus_items (
    menuid integer NOT NULL,
    itemid integer NOT NULL
);


--
-- TOC entry 225 (class 1259 OID 17275)
-- Name: options; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.options (
    id integer NOT NULL,
    name character varying(50)
);


--
-- TOC entry 226 (class 1259 OID 17278)
-- Name: options_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.options_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 226
-- Name: options_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.options_id_seq OWNED BY ${companyCode}.options.id;


--
-- TOC entry 227 (class 1259 OID 17280)
-- Name: orders; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.orders (
    id bigint NOT NULL,
    branchid integer NOT NULL,
    userid integer NOT NULL,
    total numeric(12,2) NOT NULL,
    tableid integer,
    createdat timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    paidat timestamp with time zone,
    statusid integer DEFAULT 4,
    statuscode character varying(30) DEFAULT 'EDITING'::character varying,
    note text,
    paymenttypeid integer,
    paymentdeposit bigint,
    taxcalculated boolean,
    taxamount numeric(12,2),
    totalamount numeric(12,2),
    billdata text
);


--
-- TOC entry 228 (class 1259 OID 17289)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 228
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.orders_id_seq OWNED BY ${companyCode}.orders.id;


--
-- TOC entry 229 (class 1259 OID 17291)
-- Name: orders_items; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.orders_items (
    line integer NOT NULL,
    orderid bigint NOT NULL,
    itemid integer NOT NULL,
    itemprice numeric(12,2),
    optionsprice numeric(12,2) DEFAULT 0,
    quantity integer,
    total numeric(12,2),
    note text
);


--
-- TOC entry 230 (class 1259 OID 17298)
-- Name: orders_items_options; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.orders_items_options (
    orderid bigint NOT NULL,
    itemid integer NOT NULL,
    optionid integer NOT NULL,
    line integer NOT NULL,
    optionprice numeric(12,2)
);


--
-- TOC entry 231 (class 1259 OID 17301)
-- Name: receipts; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.receipts (
    id bigint NOT NULL,
    branchid integer NOT NULL,
    createdat timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    total numeric(12,2) NOT NULL,
    paidat timestamp with time zone,
    statusid integer DEFAULT 4,
    statuscode character varying(30) DEFAULT 'EDITING'::character varying,
    note text,
    userid integer NOT NULL,
    billdata text,
    checkid bigint
);


--
-- TOC entry 232 (class 1259 OID 17310)
-- Name: receipts_ingredients; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.receipts_ingredients (
    line integer NOT NULL,
    receiptid bigint NOT NULL,
    ingredientid integer NOT NULL,
    factorydate timestamp with time zone,
    expiredat timestamp with time zone,
    price numeric(12,2) NOT NULL,
    quantity double precision NOT NULL,
    total numeric(12,2) NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 17313)
-- Name: repositories; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.repositories (
    id integer NOT NULL,
    branchid integer,
    name character varying(50),
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying
);


--
-- TOC entry 234 (class 1259 OID 17318)
-- Name: repositories_id_seq; Type: SEQUENCE; Schema: ${companyCode}; Owner: -
--

CREATE SEQUENCE ${companyCode}.repositories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 234
-- Name: repositories_id_seq; Type: SEQUENCE OWNED BY; Schema: ${companyCode}; Owner: -
--

ALTER SEQUENCE ${companyCode}.repositories_id_seq OWNED BY ${companyCode}.repositories.id;


--
-- TOC entry 235 (class 1259 OID 17320)
-- Name: repositories_ingredients; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.repositories_ingredients (
    line integer NOT NULL,
    receiptid bigint NOT NULL,
    ingredientid integer NOT NULL,
    startquantity double precision NOT NULL,
    receivedat timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    factorydate timestamp with time zone,
    expiredat timestamp with time zone,
    currentquantity double precision NOT NULL,
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    repositoryid integer NOT NULL
);


--
-- TOC entry 236 (class 1259 OID 17326)
-- Name: tables; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.tables (
    id integer NOT NULL,
    branchid integer NOT NULL,
    slots integer,
    statusid integer DEFAULT 2,
    statuscode character varying(30) DEFAULT 'AVAILABLE'::character varying,
    name character varying(30),
    floor character varying(30)
);


--
-- TOC entry 241 (class 1259 OID 17339)
-- Name: users_branches; Type: TABLE; Schema: ${companyCode}; Owner: -
--

CREATE TABLE ${companyCode}.users_branches (
    userid integer NOT NULL,
    branchid integer NOT NULL,
    companyid integer NOT NULL
);


--
-- TOC entry 3074 (class 2604 OID 17465)
-- Name: branches id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches ALTER COLUMN id SET DEFAULT nextval('${companyCode}.branches_id_seq'::regclass);


--
-- TOC entry 3081 (class 2604 OID 17466)
-- Name: checks id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.checks ALTER COLUMN id SET DEFAULT nextval('${companyCode}.checks_id_seq'::regclass);


--
-- TOC entry 3085 (class 2604 OID 17467)
-- Name: deposits id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.deposits ALTER COLUMN id SET DEFAULT nextval('${companyCode}.deposits_id_seq'::regclass);


--
-- TOC entry 3094 (class 2604 OID 17468)
-- Name: ingredients id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.ingredients ALTER COLUMN id SET DEFAULT nextval('${companyCode}.ingredients_id_seq'::regclass);


--
-- TOC entry 3095 (class 2604 OID 17469)
-- Name: ingredienttypes id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.ingredienttypes ALTER COLUMN id SET DEFAULT nextval('${companyCode}.ingredienttypes_id_seq'::regclass);


--
-- TOC entry 3098 (class 2604 OID 17470)
-- Name: items id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items ALTER COLUMN id SET DEFAULT nextval('${companyCode}.items_id_seq'::regclass);


--
-- TOC entry 3099 (class 2604 OID 17471)
-- Name: itemtypes id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.itemtypes ALTER COLUMN id SET DEFAULT nextval('${companyCode}.itemtypes_id_seq'::regclass);


--
-- TOC entry 3103 (class 2604 OID 17472)
-- Name: menus id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.menus ALTER COLUMN id SET DEFAULT nextval('${companyCode}.menus_id_seq'::regclass);


--
-- TOC entry 3104 (class 2604 OID 17473)
-- Name: options id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.options ALTER COLUMN id SET DEFAULT nextval('${companyCode}.options_id_seq'::regclass);


--
-- TOC entry 3108 (class 2604 OID 17474)
-- Name: orders id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders ALTER COLUMN id SET DEFAULT nextval('${companyCode}.orders_id_seq'::regclass);


--
-- TOC entry 3115 (class 2604 OID 17475)
-- Name: repositories id; Type: DEFAULT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories ALTER COLUMN id SET DEFAULT nextval('${companyCode}.repositories_id_seq'::regclass);


--
-- TOC entry 3124 (class 2606 OID 17482)
-- Name: branches_items branches_items_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches_items
    ADD CONSTRAINT branches_items_pk PRIMARY KEY (branchid, itemid);


--
-- TOC entry 3122 (class 2606 OID 17484)
-- Name: branches branches_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches
    ADD CONSTRAINT branches_pk PRIMARY KEY (id);


--
-- TOC entry 3126 (class 2606 OID 17486)
-- Name: checks checks_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.checks
    ADD CONSTRAINT checks_pk PRIMARY KEY (id);


--
-- TOC entry 3128 (class 2606 OID 17488)
-- Name: deposits deposits_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.deposits
    ADD CONSTRAINT deposits_pk PRIMARY KEY (id);


--
-- TOC entry 3130 (class 2606 OID 17490)
-- Name: financeaccounts financeaccounts_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.financeaccounts
    ADD CONSTRAINT financeaccounts_pk PRIMARY KEY (id);


--
-- TOC entry 3132 (class 2606 OID 17492)
-- Name: financevalidates financevalidates_pkey; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.financevalidates
    ADD CONSTRAINT financevalidates_pkey PRIMARY KEY (id);


--
-- TOC entry 3134 (class 2606 OID 17494)
-- Name: ingredients ingredients_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.ingredients
    ADD CONSTRAINT ingredients_pk PRIMARY KEY (id);


--
-- TOC entry 3136 (class 2606 OID 17496)
-- Name: ingredienttypes ingredienttypes_pkey; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.ingredienttypes
    ADD CONSTRAINT ingredienttypes_pkey PRIMARY KEY (id);


--
-- TOC entry 3140 (class 2606 OID 17498)
-- Name: items_ingredients items_ingredients_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_ingredients
    ADD CONSTRAINT items_ingredients_pk PRIMARY KEY (itemid, ingredientid);


--
-- TOC entry 3144 (class 2606 OID 17500)
-- Name: items_options_ingredients items_options_ingredients_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_options_ingredients
    ADD CONSTRAINT items_options_ingredients_pk PRIMARY KEY (itemid, optionid, ingredientid);


--
-- TOC entry 3142 (class 2606 OID 17502)
-- Name: items_options items_options_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_options
    ADD CONSTRAINT items_options_pk PRIMARY KEY (itemid, optionid);


--
-- TOC entry 3138 (class 2606 OID 17504)
-- Name: items items_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items
    ADD CONSTRAINT items_pk PRIMARY KEY (id);


--
-- TOC entry 3146 (class 2606 OID 17506)
-- Name: itemtypes itemtypes_pkey; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.itemtypes
    ADD CONSTRAINT itemtypes_pkey PRIMARY KEY (id);


--
-- TOC entry 3150 (class 2606 OID 17508)
-- Name: menus_items menus_items_pkey; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.menus_items
    ADD CONSTRAINT menus_items_pkey PRIMARY KEY (menuid, itemid);


--
-- TOC entry 3148 (class 2606 OID 17510)
-- Name: menus menus_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.menus
    ADD CONSTRAINT menus_pk PRIMARY KEY (id);


--
-- TOC entry 3152 (class 2606 OID 17512)
-- Name: options options_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.options
    ADD CONSTRAINT options_pk PRIMARY KEY (id);


--
-- TOC entry 3158 (class 2606 OID 17514)
-- Name: orders_items_options orders_items_options_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders_items_options
    ADD CONSTRAINT orders_items_options_pk PRIMARY KEY (orderid, itemid, optionid, line);


--
-- TOC entry 3156 (class 2606 OID 17516)
-- Name: orders_items orders_items_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders_items
    ADD CONSTRAINT orders_items_pk PRIMARY KEY (line, orderid, itemid);


--
-- TOC entry 3154 (class 2606 OID 17518)
-- Name: orders orders_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders
    ADD CONSTRAINT orders_pk PRIMARY KEY (id);


--
-- TOC entry 3162 (class 2606 OID 17520)
-- Name: receipts_ingredients receipts_ingredients_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts_ingredients
    ADD CONSTRAINT receipts_ingredients_pk PRIMARY KEY (line, receiptid, ingredientid);


--
-- TOC entry 3160 (class 2606 OID 17522)
-- Name: receipts receipts_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts
    ADD CONSTRAINT receipts_pk PRIMARY KEY (id);


--
-- TOC entry 3166 (class 2606 OID 17524)
-- Name: repositories_ingredients repositories_ingredients_pkey; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories_ingredients
    ADD CONSTRAINT repositories_ingredients_pkey PRIMARY KEY (repositoryid, line, receiptid, ingredientid);


--
-- TOC entry 3164 (class 2606 OID 17526)
-- Name: repositories repositories_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories
    ADD CONSTRAINT repositories_pk PRIMARY KEY (id);


--
-- TOC entry 3168 (class 2606 OID 17528)
-- Name: tables tables_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.tables
    ADD CONSTRAINT tables_pk PRIMARY KEY (id, branchid);


--
-- TOC entry 3170 (class 2606 OID 17532)
-- Name: users_branches users_branches_pk; Type: CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.users_branches
    ADD CONSTRAINT users_branches_pk PRIMARY KEY (userid, companyid, branchid);


--
-- TOC entry 3171 (class 2606 OID 17583)
-- Name: branches branches_fk_menus; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches
    ADD CONSTRAINT branches_fk_menus FOREIGN KEY (mainmenu) REFERENCES ${companyCode}.menus(id) NOT VALID;


--
-- TOC entry 3173 (class 2606 OID 17963)
-- Name: branches branches_fk_public_currencies; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches
    ADD CONSTRAINT branches_fk_public_currencies FOREIGN KEY (currencyid) REFERENCES public.currencies(id) NOT VALID;


--
-- TOC entry 3172 (class 2606 OID 17588)
-- Name: branches branches_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches
    ADD CONSTRAINT branches_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3174 (class 2606 OID 17593)
-- Name: branches_items branches_items_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches_items
    ADD CONSTRAINT branches_items_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id);


--
-- TOC entry 3175 (class 2606 OID 17598)
-- Name: branches_items branches_items_fk_items; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches_items
    ADD CONSTRAINT branches_items_fk_items FOREIGN KEY (itemid) REFERENCES ${companyCode}.items(id);


--
-- TOC entry 3176 (class 2606 OID 17603)
-- Name: branches_items branches_items_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.branches_items
    ADD CONSTRAINT branches_items_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3177 (class 2606 OID 17608)
-- Name: checks checks_fk_accounts; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.checks
    ADD CONSTRAINT checks_fk_accounts FOREIGN KEY (accountid) REFERENCES ${companyCode}.financeaccounts(id) NOT VALID;


--
-- TOC entry 3178 (class 2606 OID 17613)
-- Name: checks checks_fk_receipts; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.checks
    ADD CONSTRAINT checks_fk_receipts FOREIGN KEY (receiptid) REFERENCES ${companyCode}.receipts(id) NOT VALID;


--
-- TOC entry 3179 (class 2606 OID 17618)
-- Name: deposits deposits_fk_checks; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.deposits
    ADD CONSTRAINT deposits_fk_checks FOREIGN KEY (checkid) REFERENCES ${companyCode}.checks(id);


--
-- TOC entry 3180 (class 2606 OID 17623)
-- Name: deposits deposits_fk_financeaccounts; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.deposits
    ADD CONSTRAINT deposits_fk_financeaccounts FOREIGN KEY (accountid) REFERENCES ${companyCode}.financeaccounts(id);


--
-- TOC entry 3181 (class 2606 OID 17628)
-- Name: deposits deposits_fk_orders; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.deposits
    ADD CONSTRAINT deposits_fk_orders FOREIGN KEY (orderid) REFERENCES ${companyCode}.orders(id);


--
-- TOC entry 3182 (class 2606 OID 17633)
-- Name: deposits deposits_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.deposits
    ADD CONSTRAINT deposits_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3183 (class 2606 OID 17638)
-- Name: financeaccounts financeaccounts_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.financeaccounts
    ADD CONSTRAINT financeaccounts_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id);


--
-- TOC entry 3185 (class 2606 OID 17969)
-- Name: financeaccounts financeaccounts_fk_public_currencies; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.financeaccounts
    ADD CONSTRAINT financeaccounts_fk_public_currencies FOREIGN KEY (currencyid) REFERENCES public.currencies(id) NOT VALID;


--
-- TOC entry 3184 (class 2606 OID 17643)
-- Name: financeaccounts financeaccounts_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.financeaccounts
    ADD CONSTRAINT financeaccounts_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3186 (class 2606 OID 17648)
-- Name: financevalidates financevalidates_fk_financeaccounts; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.financevalidates
    ADD CONSTRAINT financevalidates_fk_financeaccounts FOREIGN KEY (accountid) REFERENCES ${companyCode}.financeaccounts(id);


--
-- TOC entry 3187 (class 2606 OID 17653)
-- Name: ingredients ingredients_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.ingredients
    ADD CONSTRAINT ingredients_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id) NOT VALID;


--
-- TOC entry 3188 (class 2606 OID 17658)
-- Name: ingredients ingredients_fk_types; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.ingredients
    ADD CONSTRAINT ingredients_fk_types FOREIGN KEY (typeid) REFERENCES ${companyCode}.ingredienttypes(id) NOT VALID;


--
-- TOC entry 3189 (class 2606 OID 17663)
-- Name: items items_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items
    ADD CONSTRAINT items_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3190 (class 2606 OID 17668)
-- Name: items items_fk_types; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items
    ADD CONSTRAINT items_fk_types FOREIGN KEY (typeid) REFERENCES ${companyCode}.itemtypes(id) NOT VALID;


--
-- TOC entry 3191 (class 2606 OID 17673)
-- Name: items_ingredients items_ingredients_fk_ingredients; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_ingredients
    ADD CONSTRAINT items_ingredients_fk_ingredients FOREIGN KEY (ingredientid) REFERENCES ${companyCode}.ingredients(id);


--
-- TOC entry 3192 (class 2606 OID 17678)
-- Name: items_ingredients items_ingredients_fk_items; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_ingredients
    ADD CONSTRAINT items_ingredients_fk_items FOREIGN KEY (itemid) REFERENCES ${companyCode}.items(id);


--
-- TOC entry 3193 (class 2606 OID 17683)
-- Name: items_options items_options_fk_items; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_options
    ADD CONSTRAINT items_options_fk_items FOREIGN KEY (itemid) REFERENCES ${companyCode}.items(id);


--
-- TOC entry 3194 (class 2606 OID 17688)
-- Name: items_options items_options_fk_options; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_options
    ADD CONSTRAINT items_options_fk_options FOREIGN KEY (optionid) REFERENCES ${companyCode}.options(id);


--
-- TOC entry 3195 (class 2606 OID 17693)
-- Name: items_options_ingredients items_options_ingredients_fk_ingredients; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_options_ingredients
    ADD CONSTRAINT items_options_ingredients_fk_ingredients FOREIGN KEY (ingredientid) REFERENCES ${companyCode}.ingredients(id);


--
-- TOC entry 3196 (class 2606 OID 17698)
-- Name: items_options_ingredients items_options_ingredients_fk_items_options; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.items_options_ingredients
    ADD CONSTRAINT items_options_ingredients_fk_items_options FOREIGN KEY (itemid, optionid) REFERENCES ${companyCode}.items_options(itemid, optionid) NOT VALID;


--
-- TOC entry 3197 (class 2606 OID 17703)
-- Name: menus menus_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.menus
    ADD CONSTRAINT menus_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3198 (class 2606 OID 17708)
-- Name: menus_items menus_items_fk_items; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.menus_items
    ADD CONSTRAINT menus_items_fk_items FOREIGN KEY (itemid) REFERENCES ${companyCode}.items(id);


--
-- TOC entry 3199 (class 2606 OID 17713)
-- Name: menus_items menus_items_fk_menus; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.menus_items
    ADD CONSTRAINT menus_items_fk_menus FOREIGN KEY (menuid) REFERENCES ${companyCode}.menus(id);


--
-- TOC entry 3200 (class 2606 OID 17718)
-- Name: orders orders_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders
    ADD CONSTRAINT orders_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id);


--
-- TOC entry 3204 (class 2606 OID 17934)
-- Name: orders orders_fk_deposits; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders
    ADD CONSTRAINT orders_fk_deposits FOREIGN KEY (paymentdeposit) REFERENCES ${companyCode}.deposits(id) NOT VALID;


--
-- TOC entry 3201 (class 2606 OID 17723)
-- Name: orders orders_fk_public_paymenttypes; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders
    ADD CONSTRAINT orders_fk_public_paymenttypes FOREIGN KEY (paymenttypeid) REFERENCES public.paymenttypes(id) NOT VALID;


--
-- TOC entry 3202 (class 2606 OID 17728)
-- Name: orders orders_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders
    ADD CONSTRAINT orders_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3203 (class 2606 OID 17733)
-- Name: orders orders_fk_tables; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders
    ADD CONSTRAINT orders_fk_tables FOREIGN KEY (tableid, branchid) REFERENCES ${companyCode}.tables(id, branchid) NOT VALID;


--
-- TOC entry 3205 (class 2606 OID 17738)
-- Name: orders_items orders_items_fk_items; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders_items
    ADD CONSTRAINT orders_items_fk_items FOREIGN KEY (itemid) REFERENCES ${companyCode}.items(id) NOT VALID;


--
-- TOC entry 3206 (class 2606 OID 17743)
-- Name: orders_items orders_items_fk_orders; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders_items
    ADD CONSTRAINT orders_items_fk_orders FOREIGN KEY (orderid) REFERENCES ${companyCode}.orders(id) NOT VALID;


--
-- TOC entry 3207 (class 2606 OID 17748)
-- Name: orders_items_options orders_items_options_pk_items_options; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders_items_options
    ADD CONSTRAINT orders_items_options_pk_items_options FOREIGN KEY (itemid, optionid) REFERENCES ${companyCode}.items_options(itemid, optionid);


--
-- TOC entry 3208 (class 2606 OID 17753)
-- Name: orders_items_options orders_items_options_pk_orders_items; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.orders_items_options
    ADD CONSTRAINT orders_items_options_pk_orders_items FOREIGN KEY (orderid, itemid, line) REFERENCES ${companyCode}.orders_items(orderid, itemid, line);


--
-- TOC entry 3209 (class 2606 OID 17758)
-- Name: receipts receipts_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts
    ADD CONSTRAINT receipts_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id);


--
-- TOC entry 3211 (class 2606 OID 17974)
-- Name: receipts receipts_fk_checks; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts
    ADD CONSTRAINT receipts_fk_checks FOREIGN KEY (checkid) REFERENCES ${companyCode}.checks(id) NOT VALID;


--
-- TOC entry 3210 (class 2606 OID 17763)
-- Name: receipts receipts_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts
    ADD CONSTRAINT receipts_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3212 (class 2606 OID 17768)
-- Name: receipts_ingredients receipts_ingredients_fk_ingredients; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts_ingredients
    ADD CONSTRAINT receipts_ingredients_fk_ingredients FOREIGN KEY (ingredientid) REFERENCES ${companyCode}.ingredients(id);


--
-- TOC entry 3213 (class 2606 OID 17773)
-- Name: receipts_ingredients receipts_ingredients_fk_receipts; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.receipts_ingredients
    ADD CONSTRAINT receipts_ingredients_fk_receipts FOREIGN KEY (receiptid) REFERENCES ${companyCode}.receipts(id);


--
-- TOC entry 3214 (class 2606 OID 17778)
-- Name: repositories repositories_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories
    ADD CONSTRAINT repositories_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id);


--
-- TOC entry 3215 (class 2606 OID 17783)
-- Name: repositories_ingredients repositories_ingredients_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories_ingredients
    ADD CONSTRAINT repositories_ingredients_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3216 (class 2606 OID 17788)
-- Name: repositories_ingredients repositories_ingredients_fk_receipts_ingredients; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories_ingredients
    ADD CONSTRAINT repositories_ingredients_fk_receipts_ingredients FOREIGN KEY (line, receiptid, ingredientid) REFERENCES ${companyCode}.receipts_ingredients(line, receiptid, ingredientid);


--
-- TOC entry 3217 (class 2606 OID 17793)
-- Name: repositories_ingredients repositories_ingredients_fk_repositories; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.repositories_ingredients
    ADD CONSTRAINT repositories_ingredients_fk_repositories FOREIGN KEY (repositoryid) REFERENCES ${companyCode}.repositories(id) NOT VALID;


--
-- TOC entry 3218 (class 2606 OID 17798)
-- Name: tables tables_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.tables
    ADD CONSTRAINT tables_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id);


--
-- TOC entry 3219 (class 2606 OID 17803)
-- Name: tables tables_fk_public_status; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.tables
    ADD CONSTRAINT tables_fk_public_status FOREIGN KEY (statusid) REFERENCES public.status(id);


--
-- TOC entry 3220 (class 2606 OID 17808)
-- Name: users_branches users_branches_fk_branches; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.users_branches
    ADD CONSTRAINT users_branches_fk_branches FOREIGN KEY (branchid) REFERENCES ${companyCode}.branches(id) NOT VALID;


--
-- TOC entry 3221 (class 2606 OID 17813)
-- Name: users_branches users_branches_fk_public_users; Type: FK CONSTRAINT; Schema: ${companyCode}; Owner: -
--

ALTER TABLE ONLY ${companyCode}.users_branches
    ADD CONSTRAINT users_branches_fk_public_users FOREIGN KEY (userid, companyid) REFERENCES public.users(id, companyid);


-- Completed on 2021-03-16 16:29:39

--
-- PostgreSQL database dump complete
--

`
}
