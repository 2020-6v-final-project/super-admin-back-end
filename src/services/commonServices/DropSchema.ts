export function getDropScript(companyCode: string, companyId: number): string {
  companyCode = companyCode.toLowerCase();
  return `DELETE FROM public.companies_roles WHERE companyid = ${companyId};
DELETE FROM public.businesscontracts_payments WHERE companyid = ${companyId};
DELETE FROM public.businesscontracts WHERE companyid = ${companyId};
DELETE FROM public.users_roles WHERE companyid = ${companyId};
DELETE FROM public.accounts WHERE companyid = ${companyId};
DELETE FROM public.users WHERE companyid = ${companyId};
DELETE FROM public.companies WHERE id = ${companyId};
  DROP SCHEMA ${companyCode} CASCADE;`
}
