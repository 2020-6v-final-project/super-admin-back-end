import {IAccount} from '../../models/IAccount';
import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IAccountTableName = 'public.accounts';

export default {
  IAccountTableName,
  async createSequenceWithCompanyId(companyId: number, inputClient?: PoolClient) {
    return await DatabaseCommand.createSequenceForId('integer', IAccountTableName + companyId, inputClient)
  },
  async dropSequenceWithCompanyId(companyId: number, inputClient?: PoolClient) {
    await DatabaseCommand.dropSequenceForId(IAccountTableName + companyId, inputClient);
  },
  async nextId(companyId: number, inputClient?: PoolClient): Promise<number> {
    return await DatabaseCommand.getNextSequenceId(`${IAccountTableName}${companyId}_id_seq`, inputClient);
  },
  async getAccounts(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IAccount[]> {
    const rows = await DatabaseCommand.select(['*'], IAccountTableName, condition, lockStrength, inputClient);
    return rows as IAccount[];
  },
  async deleteAccount(account: IAccount, inputClient?: PoolClient): Promise<IAccount[]> {
    const rows = await DatabaseCommand.delete(IAccountTableName, [{
      id: account.id, companyid: account.companyid
    }], inputClient);
    return rows as IAccount[];
  },
  /**
   * Create new account
   * @param account
   * @param inputClient
   */
  async createAccount(account: { id: number, companyid: number }, inputClient?: PoolClient): Promise<IAccount> {
    account.id = await this.nextId(account.companyid, inputClient);
    return await DatabaseCommand.insert(account, IAccountTableName, inputClient) as Promise<IAccount>;
  },
  /**
   *
   * @param account
   * @param inputClient
   */
  async updateAccount(account: IAccount, inputClient?: PoolClient): Promise<IAccount | null> {
    const rows = await DatabaseCommand.update(account, IAccountTableName, [{id: account.id, companyid: account.companyid}], inputClient);
    if (rows.length) return rows[0] as IAccount;
    return null;
  }
}
