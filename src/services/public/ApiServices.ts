import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IApi} from "../../models/IApi";

const IApiTableName = 'public.apis';

export default {
  async getApis(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IApi[]> {
    const rows = await DatabaseCommand.select(['*'], IApiTableName, condition, lockStrength, inputClient);
    return rows as IApi[];
  },
  async createApi(api: object, inputClient?: PoolClient): Promise<IApi> {
    return await DatabaseCommand.insert(api, IApiTableName, inputClient) as Promise<IApi>;
  },
  async updateApi(api: IApi, inputClient?: PoolClient): Promise<IApi | null> {
    const rows = await DatabaseCommand.update(api, IApiTableName, [{id: api.id}], inputClient);
    if (rows.length) return rows[0] as IApi;
    return null;
  },
  async deleteApi(api: IApi, inputClient?: PoolClient): Promise<IApi | null> {
    const rows = await DatabaseCommand.delete(IApiTableName, [{id: api.id}], inputClient)
    if (!rows.length) return null;
    return rows[0] as IApi;
  },
  async getApisOfPermissionIds(permissionIds: number[], inputClient?: PoolClient): Promise<IApi[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM public.apis WHERE id IN
(SELECT distinct apiid FROM public.permissions_apis WHERE permissionid IN (${permissionIds.join(',')})) FOR KEY SHARE`, [], inputClient);
    return result.rows as IApi[];
  }
}
