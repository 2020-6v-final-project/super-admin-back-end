import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IBusinessContractPayment} from "../../models/IBusinessContractPayment";
import {IBusinessContract} from "../../models/IBusinessContract";
import BusinessContractServices from "./BusinessContractServices";

const IBusinessContractPaymentTableName = 'public.businesscontracts_payments';

export default {
  IBusinessContractPaymentTableName,
  async getBusinessContractPayments(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IBusinessContractPayment[]> {
    const rows = await DatabaseCommand.select(['*'], IBusinessContractPaymentTableName, condition, lockStrength, inputClient);
    return rows as IBusinessContractPayment[];
  },
  async createBusinessContractPayment(payment: object, inputClient?: PoolClient): Promise<IBusinessContractPayment> {
    return await DatabaseCommand.insert(payment, IBusinessContractPaymentTableName, inputClient) as Promise<IBusinessContractPayment>;
  },
  async updateBusinessContractPayment(payment: IBusinessContractPayment, inputClient?: PoolClient): Promise<IBusinessContractPayment | null> {
    const rows = await DatabaseCommand.update(payment, IBusinessContractPaymentTableName, [{
      id: payment.id, businesscontractid: payment.businesscontractid, companyid: payment.companyid
    }], inputClient);
    if (rows.length) return rows[0] as IBusinessContractPayment;
    return null;
  },
  async deletePayment(payment: IBusinessContractPayment, inputClient?: PoolClient): Promise<IBusinessContractPayment | null> {
    const rows = await DatabaseCommand.delete(IBusinessContractPaymentTableName, [{id: payment.id}], inputClient);
    if (!rows.length) return null
    return rows[0] as IBusinessContractPayment;
  },
  async createPaymentForContract(contract: IBusinessContract, inputClient?: PoolClient): Promise<IBusinessContractPayment> {
    const payment = await this.createBusinessContractPayment({
      businesscontractid: contract.id,
      companyid: contract.companyid,
      total: contract.price * contract.totalbranch * contract.quantity
    }, inputClient);
    if (!contract.total) {
      contract.total = payment.total;
      await BusinessContractServices.updateBusinessContract(contract, inputClient);
    }
    return payment;
  }
}
