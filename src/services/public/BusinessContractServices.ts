import { IBusinessContract } from '../../models/IBusinessContract';
import { PoolClient } from 'pg';
import DatabaseCommand from '../../utils/DatabaseCommand';

const IBusinessContractTableName = 'public.businesscontracts';

export default {
    IBusinessContractTableName,
    async createSequenceWithCompanyId(companyId: number, inputClient?: PoolClient) {
        await DatabaseCommand.createSequenceForId('integer', IBusinessContractTableName + companyId, inputClient)
    },
    async dropSequenceWithCompanyId(companyId: number, inputClient?: PoolClient) {
        await DatabaseCommand.dropSequenceForId(IBusinessContractTableName + companyId, inputClient);
    },
    async nextId(companyId: number, inputClient?: PoolClient): Promise<number> {
        return await DatabaseCommand.getNextSequenceId(`${IBusinessContractTableName}${companyId}_id_seq`, inputClient);
    },
    /**
     * Create new BusinessContract
     * @param businessContract
     * @param inputClient
     */
    async createBusinessContract(businessContract: { id: number, companyid: number }, inputClient?: PoolClient): Promise<IBusinessContract> {
        businessContract.id = await this.nextId(businessContract.companyid, inputClient);
        const rows = await DatabaseCommand.insert(businessContract, IBusinessContractTableName, inputClient);
        return rows as IBusinessContract;
    },
    /**
     * get BusinessContract(s)
     * @param condition
     * @param lockStrength
     * @param inputClient
     */
    async getBusinessContracts(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IBusinessContract[]> {
        const rows = await DatabaseCommand.select(['*'], IBusinessContractTableName, condition, lockStrength, inputClient);
        return rows as IBusinessContract[];
    },
    /**
     * update BusinessContract by id
     * @param businessContract
     * @param inputClient
     */
    async updateBusinessContract(businessContract: IBusinessContract, inputClient?: PoolClient): Promise<IBusinessContract | null> {
        const rows = await DatabaseCommand.update(businessContract, IBusinessContractTableName, [{ id: businessContract.id, companyid: businessContract.companyid }], inputClient);
        return rows[0] as IBusinessContract;
    },
}
