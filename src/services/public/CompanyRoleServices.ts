import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {ICompanyRole} from "../../models/ICompanyRole";

const ICompanyRoleTableName = 'public.companies_roles';

export default {
  async getCompanyRoles(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<ICompanyRole[]> {
    const rows = await DatabaseCommand.select(['*'], ICompanyRoleTableName, condition, lockStrength, inputClient);
    return rows as ICompanyRole[];
  },
  async createCompanyRole(companyRole: object, inputClient?: PoolClient): Promise<ICompanyRole> {
    return await DatabaseCommand.insert(companyRole, ICompanyRoleTableName, inputClient) as Promise<ICompanyRole>;
  },
  async deleteCompanyRole(companyRole: ICompanyRole, inputClient?: PoolClient): Promise<ICompanyRole | null> {
    const rows = await DatabaseCommand.delete(ICompanyRoleTableName, [{roleid: companyRole.roleid, companyid: companyRole.companyid}], inputClient);
    if (!rows.length) return null;
    return rows[0] as ICompanyRole;
  }
}
