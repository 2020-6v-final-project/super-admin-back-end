import {ICompany} from "../../models/ICompany";
import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {getCreateScript} from "../commonServices/CreateSchema";
import {getDropScript} from "../commonServices/DropSchema";
import {getAlterScript} from "../commonServices/ChangeCompanyCode";

const ICompanyTableName = 'public.companies';

export default {
  ICompanyTableName,
  ////==feat-company-api-get-list-and-detail==/
  getCompanies: async (condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<ICompany[]> => {
    const rows = await DatabaseCommand.select(['*'], ICompanyTableName, condition, lockStrength, inputClient);
    return rows as ICompany[];
  },
  createCompany: async (company: {code: string}, inputClient?: PoolClient): Promise<ICompany> => {
    const insertedCompany = await DatabaseCommand.insert(company, ICompanyTableName, inputClient) as Promise<ICompany>;
    const createScript = getCreateScript(company.code);
    await DatabaseCommand.customQuery(createScript, [], inputClient);
    return insertedCompany;
  },
  updateCompany: async (company: ICompany, oldCode?: string, inputClient?: PoolClient): Promise<ICompany | null> => {
    const rows = await DatabaseCommand.update(company, ICompanyTableName, [{id: company.id}], inputClient);
    if (rows.length) {
      const updatedCompany = rows[0] as ICompany;
      if (oldCode && company.code !== oldCode) {
        const alterScript = getAlterScript(company.id, oldCode, updatedCompany.code);
        await DatabaseCommand.customQuery(alterScript, [], inputClient);
      }
      return updatedCompany;
    }
    return null;
  },
  async deleteCompany(company: ICompany, inputClient?: PoolClient) {
    const dropScript = getDropScript(company.code, company.id);
    await DatabaseCommand.customQuery(dropScript, [], inputClient);
  }
};
