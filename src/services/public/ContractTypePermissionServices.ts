import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IContractTypePermission} from "../../models/IContractTypePermission";

const IContractTypePermissionTableName = 'public.contracttypes_permissions';

export default {
  async getTypePermissions(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IContractTypePermission[]> {
    const rows = await DatabaseCommand.select(['*'], IContractTypePermissionTableName, condition, lockStrength, inputClient);
    return rows as IContractTypePermission[];
  },
  async createTypePermission(typeRole: object, inputClient?: PoolClient): Promise<IContractTypePermission> {
    return await DatabaseCommand.insert(typeRole, IContractTypePermissionTableName, inputClient) as Promise<IContractTypePermission>;
  },
  async deleteTypePermissions(condition: object[], inputClient?: PoolClient): Promise<IContractTypePermission[]> {
    const rows = await DatabaseCommand.delete(IContractTypePermissionTableName, condition, inputClient);
    return rows as IContractTypePermission[];
  }
}
