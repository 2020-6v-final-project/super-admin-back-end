import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IContractType} from "../../models/IContractType";

const IContractTypeTableName = 'public.contracttypes';

export default {
  async getContractTypes(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IContractType[]> {
    const rows = await DatabaseCommand.select(['*'], IContractTypeTableName, condition, lockStrength, inputClient);
    return rows as IContractType[];
  },
  async createContractType(type: object, inputClient?: PoolClient): Promise<IContractType> {
    return await DatabaseCommand.insert(type, IContractTypeTableName, inputClient) as Promise<IContractType>;
  },
  async updateContractType(type: IContractType, inputClient?: PoolClient): Promise<IContractType | null> {
    const rows = await DatabaseCommand.update(type, IContractTypeTableName, [{id: type.id}], inputClient);
    if (rows.length) return rows[0] as IContractType;
    return null;
  },
  async deleteContractType(type: IContractType, inputClient?: PoolClient): Promise<IContractType | null> {
    const rows = await DatabaseCommand.delete(IContractTypeTableName, [{id: type.id}], inputClient)
    if (!rows.length) return null;
    return rows[0] as IContractType;
  },
  async getAllPossibleContractType(lockStrength?: string, inputClient?: PoolClient): Promise<IContractType[]> {
    const rows = await DatabaseCommand.select(['*'], IContractTypeTableName, [], lockStrength, inputClient);
    return rows.filter(row => {
      const type = row as IContractType;
      return type.id !== 5;
    }) as IContractType[];
  }
}
