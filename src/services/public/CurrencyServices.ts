import DatabaseCommand from "../../utils/DatabaseCommand";
import {ICurrency} from "../../models/ICurrency";

const ICurrencyTableName = `public.currencies`;

export default {
  async getCurrencies(condition: object[]) {
    return await DatabaseCommand.select(['*'], ICurrencyTableName, condition) as ICurrency[];
  }
}
