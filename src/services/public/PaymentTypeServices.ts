import {PoolClient} from "pg";
import {IPaymentType} from "../../models/IPaymentType";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IPaymentTypeTableName = 'public.paymenttypes'

export default {
  async getPaymentTypes(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IPaymentType[]> {
    return await DatabaseCommand.select(['*'], IPaymentTypeTableName, condition, lockStrength, inputClient) as IPaymentType[];
  },
  async createPaymentType(paymentType: object, inputClient?: PoolClient): Promise<IPaymentType> {
    return await DatabaseCommand.insert(paymentType, IPaymentTypeTableName, inputClient) as IPaymentType;
  },
  async updatePaymentTypes(condition: object[], updateObject: object, inputClient?: PoolClient): Promise<IPaymentType[]> {
    return await DatabaseCommand.update(updateObject, IPaymentTypeTableName, condition, inputClient) as IPaymentType[];
  },
  async deletePaymentTypes(condition: object[], inputClient?: PoolClient): Promise<IPaymentType[]> {
    return await DatabaseCommand.delete(IPaymentTypeTableName, condition, inputClient) as IPaymentType[];
  }
}
