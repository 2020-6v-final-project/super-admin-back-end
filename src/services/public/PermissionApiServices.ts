import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IPermissionApi} from "../../models/IPermissionApi";

const IPermissionApiTableName = 'public.permissions_apis';

export default {
  async getPermissionApis(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IPermissionApi[]> {
    const rows = await DatabaseCommand.select(['*'], IPermissionApiTableName, condition, lockStrength, inputClient);
    return rows as IPermissionApi[];
  },
  async createPermissionApi(permissionApi: object, inputClient?: PoolClient): Promise<IPermissionApi> {
    return await DatabaseCommand.insert(permissionApi, IPermissionApiTableName, inputClient) as Promise<IPermissionApi>;
  },
  async deletePermissionApi(permissionApi: IPermissionApi, inputClient?: PoolClient): Promise<IPermissionApi | null> {
    const rows = await DatabaseCommand.delete(IPermissionApiTableName, [{permissionid: permissionApi.permissionid, apiid: permissionApi.apiid}], inputClient)
    if (!rows.length) return null;
    return rows[0] as IPermissionApi;
  }
}
