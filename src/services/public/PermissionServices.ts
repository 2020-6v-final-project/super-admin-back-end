import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IPermission} from "../../models/IPermission";

const IPermissionTableName = 'public.permissions';

export default {
  async getPermissions(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IPermission[]> {
    const rows = await DatabaseCommand.select(['*'], IPermissionTableName, condition, lockStrength, inputClient);
    return rows as IPermission[];
  },
  async createPermission(permission: object, inputClient?: PoolClient): Promise<IPermission> {
    return await DatabaseCommand.insert(permission, IPermissionTableName, inputClient) as Promise<IPermission>;
  },
  async updatePermission(permission: IPermission, inputClient?: PoolClient): Promise<IPermission | null> {
    const rows = await DatabaseCommand.update(permission, IPermissionTableName, [{id: permission.id}], inputClient);
    if (rows.length) return rows[0] as IPermission;
    return null;
  },
  async deletePermission(permission: IPermission, inputClient?: PoolClient): Promise<IPermission | null> {
    const rows = await DatabaseCommand.delete(IPermissionTableName, [{id: permission.id}], inputClient)
    if (!rows.length) return null;
    return rows[0] as IPermission;
  },
  async getPermissionsOfRoleIds(roleIds: number[], inputClient?: PoolClient): Promise<IPermission[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM public.permissions WHERE id IN
(SELECT distinct permissionid FROM public.roles_permissions WHERE roleid IN (${roleIds.join(',')})) FOR KEY SHARE`, [], inputClient);
    return result.rows as IPermission[];
  },
  async getPermissionsOfTypeId(typeId: number, inputClient?: PoolClient): Promise<IPermission[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM public.permissions WHERE id IN
(SELECT permissionid FROM public.contracttypes_permissions WHERE contracttypeid = ${typeId}) FOR KEY SHARE`, [], inputClient);
    return result.rows as IPermission[];
  },
  async getPermissionsOfRoleIdsFilterWithTypeId(roleIds: number[], typeId: number, server: string, inputClient?: PoolClient): Promise<IPermission[]> {
    const result = await DatabaseCommand.customQuery(`SELECT * FROM public.permissions WHERE server = '${server}' AND id IN
(SELECT distinct permissionid FROM public.roles_permissions WHERE roleid IN (${roleIds.join(',')}) INTERSECT
SELECT distinct permissionid FROM public.contracttypes_permissions WHERE contracttypeid = ${typeId}) FOR KEY SHARE`, [], inputClient);
    return result.rows as IPermission[];
  }
}
