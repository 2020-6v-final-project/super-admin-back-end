import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IRolePermission} from "../../models/IRolePermission";

const IRolePermissionTableName = 'public.roles_permissions';

export default {
  async getRolePermissions(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IRolePermission[]> {
    const rows = await DatabaseCommand.select(['*'], IRolePermissionTableName, condition, lockStrength, inputClient);
    return rows as IRolePermission[];
  },
  async createRolePermission(rolePermission: object, inputClient?: PoolClient): Promise<IRolePermission> {
    return await DatabaseCommand.insert(rolePermission, IRolePermissionTableName, inputClient) as Promise<IRolePermission>;
  },
  async deleteRolePermission(rolePermission: IRolePermission, inputClient?: PoolClient): Promise<IRolePermission | null> {
    const rows = await DatabaseCommand.delete(IRolePermissionTableName, [{permissionid: rolePermission.permissionid, roleid: rolePermission.roleid}], inputClient);
    if (!rows.length) return null;
    return rows[0] as IRolePermission;
  }
}
