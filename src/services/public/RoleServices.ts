import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import {IRole} from "../../models/IRole";
import {IUser} from "../../models/IUser";
import UserRoleServices from "./UserRoleServices";
import CompanyRoleServices from "./CompanyRoleServices";

const IRoleTableName = 'public.roles';

export default {
  IRoleTableName,
  async createRole(role: object, inputClient?: PoolClient): Promise<IRole> {
    return await DatabaseCommand.insert(role, IRoleTableName, inputClient) as Promise<IRole>;
  },
  async updateRole(role: IRole, inputClient?: PoolClient): Promise<IRole | null> {
    const rows = await DatabaseCommand.update(role, IRoleTableName, [{id: role.id}], inputClient);
    if (rows.length) return rows[0] as IRole;
    return null;
  },
  async deleteRole(role: IRole, inputClient?: PoolClient): Promise<IRole | null> {
    const rows = await DatabaseCommand.delete(IRoleTableName, [{id: role.id}], inputClient)
    if (!rows.length) return null;
    return rows[0] as IRole;
  },
  async getRoles(condition: object[], companyId?: number, lockStrength?: string, inputClient?: PoolClient): Promise<IRole[]> {
    const rows = await DatabaseCommand.select(['*'], IRoleTableName, condition, lockStrength, inputClient);
    const roles = rows as IRole[];
    if (companyId) {
      const companyRoles = await CompanyRoleServices.getCompanyRoles([{
        companyid: companyId
      }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient);
      roles.filter(role => {
        return role.isprimary || (companyRoles.find(companyRole => companyRole.companyid === companyId) !== undefined);
      });
    }
    return roles;
  },
  async getRolesOfUser(thisUser: IUser, lockStrength?: string, inputClient?: PoolClient): Promise<IRole[]> {
    return await Promise.all((await UserRoleServices.getUserRoles([{
      userid: thisUser.id, companyid: thisUser.companyid
    }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient)).map(async userRole => {
      return (await this.getRoles([{
        id: userRole.roleid
      }], thisUser.companyid, lockStrength, inputClient))[0];
    }));
  }
}
