import {IStatus} from "../../models/IStatus";
import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IStatusTableName = 'public.status';

export default {
  IStatusTableName,
  async getStatus(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IStatus[]> {
    const rows = await DatabaseCommand.select(['*'], IStatusTableName, condition, lockStrength, inputClient);
    return rows as IStatus[];
  },
  async setStatusCodeToObjects(tableName: string, statusCode: string, condition: object[], inputClient?: PoolClient): Promise<{ statusid: number, statuscode: string }[]> {
    const rows = await this.getStatus([{code: statusCode}], DatabaseCommand.lockStrength.SHARE, inputClient);
    if (!rows.length) return [];
    const thisStatus = rows[0] as IStatus;
    const updatedRows = await DatabaseCommand.update({statusid: thisStatus.id, statuscode: thisStatus.code}, tableName, condition, inputClient);
    return updatedRows as { statusid: number, statuscode: string }[];
  }
}
