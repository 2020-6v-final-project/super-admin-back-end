import {IUserRole} from "../../models/IUserRole";
import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";

const IUserRoleTableName = 'public.users_roles';

export default {
  IUserRoleTableName,
  async getUserRoles(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IUserRole[]> {
    const rows = await DatabaseCommand.select(['*'], IUserRoleTableName, condition, lockStrength, inputClient);
    return rows as IUserRole[];
  },
  async addUserRole(userRole: IUserRole, inputClient?: PoolClient): Promise<IUserRole> {
    return await DatabaseCommand.insert(userRole, IUserRoleTableName, inputClient) as Promise<IUserRole>;
  },
  async removeUserRole(userRole: IUserRole, inputClient?: PoolClient): Promise<IUserRole | null> {
    const rows = await DatabaseCommand.delete(IUserRoleTableName, [userRole], inputClient);
    if (rows.length) return rows[0] as IUserRole;
    return null;
  }
}
