import {IUser} from '../../models/IUser';
import {PoolClient} from "pg";
import DatabaseCommand from "../../utils/DatabaseCommand";
import Functions from "../../utils/Functions";
import AccountServices from "./AccountServices";
import {setPassword} from "../../models/IAccount";

const IUserTableName = 'public.users';

export default {
  IUserTableName,
  async createSequenceWithCompanyId(companyId: number, inputClient?: PoolClient) {
    return await DatabaseCommand.createSequenceForId('integer', IUserTableName + companyId.toString(), inputClient);
  },
  async dropSequenceWithCompanyId(companyId: number, inputClient?: PoolClient) {
    await DatabaseCommand.dropSequenceForId(IUserTableName + companyId, inputClient);
  },
  async nextId(companyId: number, inputClient?: PoolClient): Promise<number> {
    return await DatabaseCommand.getNextSequenceId(`${IUserTableName}${companyId}_id_seq`, inputClient);
  },
  async getUsers(condition: object[], lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, condition, lockStrength, inputClient);
    return rows as IUser[];
  },
  async createUser(user: { id: number, companyid: number }, inputClient?: PoolClient): Promise<IUser> {
    user.id = await this.nextId(user.companyid, inputClient);
    return await DatabaseCommand.insert(user, IUserTableName, inputClient) as Promise<IUser>;
  },
  async createUserWithAccount(user: { id: number, companyid: number, companycode: string }, inputClient?: PoolClient) {
    const insertedUser = await this.createUser(user, inputClient);
    let preUsername;
    const companyCodeLowerCase = insertedUser.companycode.toLowerCase();
    preUsername = `${insertedUser.lastname[0] || ''}${insertedUser.middlename[0] || ''}${insertedUser.firstname}`;
    preUsername = preUsername.toLowerCase();
    preUsername = Functions.nonAccentVietnamese(preUsername);
    let tempUsername = `${preUsername}@${companyCodeLowerCase}`;
    let accounts = await AccountServices.getAccounts([{
      username: tempUsername,
      companyid: insertedUser.companyid
    }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient);
    let i = 1;
    while (accounts.length) {
      preUsername = preUsername + i;
      tempUsername = `${preUsername}@${companyCodeLowerCase}`;
      accounts = await AccountServices.getAccounts([{
        username: tempUsername,
        companyid: insertedUser.companyid
      }], DatabaseCommand.lockStrength.KEY_SHARE, inputClient);
      i = i + 1;
    }
    const password = companyCodeLowerCase + preUsername;
    let newAccount = {
      id: 0,
      userid: insertedUser.id,
      username: tempUsername,
      hash: '',
      salt: '',
      companyid: insertedUser.companyid,
    };
    setPassword(password, newAccount);
    const createdAccount = await AccountServices.createAccount(newAccount, inputClient);
    return {
      newUser: insertedUser,
      newAccount: {
        username: createdAccount.username,
        password
      }
    }
  },
  /**
   * Update / save user to database
   * @param user The user query from database
   * @param inputClient
   */
  async updateOrSaveUser(user: IUser, inputClient?: PoolClient): Promise<IUser | null> {
    const rows = await DatabaseCommand.update(user, IUserTableName, [{
      id: user.id,
      companyid: user.companyid
    }], inputClient);
    if (rows.length > 0) return rows[0] as IUser;
    return null;
  },
  async getAllUser(lockStrength?: string, inputClient?: PoolClient): Promise<IUser[]> {
    const rows = await DatabaseCommand.select(['*'], IUserTableName, [], lockStrength, inputClient);
    return rows as IUser[];
  }
}
