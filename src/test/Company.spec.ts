// import {expect} from 'chai';
// import CompanyServices from '../services/CompanyServices';
// import UserServices from '../services/UserServices';
// import {companySample} from './sampleTestCase';
// import {ICompany} from '../models/ICompany';
// import {PoolClient} from "pg";
// import DatabaseCommand from "../utils/DatabaseCommand";
//
// function isICompany(object: any): object is ICompany {
//   return 'id' in object;
// }
//
// let company = companySample;
//
// let client: PoolClient;
//
// describe('Company services Test', () => {
//   before(async function () {
//     client = await DatabaseCommand.newClient();
//     await DatabaseCommand.beginTransaction(client);
//   });
//
//   after(async function () {
//     await DatabaseCommand.rollbackTransaction(client);
//     client.release();
//   });
//
//   try {
//     it('#Create company success', async () => {
//       const createCompany = company as any;
//       delete createCompany.owner
//       const result = await CompanyServices.createCompany(createCompany, client);
//       expect(result).to.be.an('object').not.empty;
//       if (isICompany(result)) company = {...company, id: result.id};
//     });
//
//     it('#Get company by company id success', async () => {
//       const result = await CompanyServices.getCompanyById(company.id, undefined, client);
//       expect(result).to.be.an('object');
//     });
//
//     it('#Get company by company id fail', async () => {
//       const result = await CompanyServices.getCompanyById(-1, undefined, client);
//       expect(result).to.be.null;
//     });
//
//     it('#Get all company success', async () => {
//       const result = await CompanyServices.getCompanies([], undefined, client);
//       expect(result).to.be.an('array');
//     });
//
//     it('#Get company by company code success', async () => {
//       const result = await CompanyServices.getCompanyByCode(company.code, undefined, client);
//       expect(result).to.be.an('object').not.empty;
//     });
//
//     it('#Get company by company code fail', async () => {
//       const result = await CompanyServices.getCompanyByCode("A_NAME_CONSTANCE_MORE_THAN_15_CHARACTERS", undefined, client);
//       expect(result).to.be.null;
//     });
//
//     it('#Update or save company success', async () => {
//       const result = await CompanyServices.updateOrSaveCompany({
//         ...company,
//         name: 'ABCXYZ'
//       }, client);
//       expect(result).to.be.an('object');
//       expect(result?.name).equal('ABCXYZ');
//     });
//
//     it('#Update or save company fail', async () => {
//       const result = await CompanyServices.updateOrSaveCompany({
//         ...company,
//         id: -1
//       }, client);
//       expect(result).to.be.null;
//     });
//   } catch (e) {
//     console.log(e)
//     DatabaseCommand.rollbackTransaction(client).then(result => {
//       client.release();
//     });
//   }
// });
