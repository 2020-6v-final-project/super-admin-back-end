// import { expect } from 'chai';
// import RoleServices from '../services/RoleServices';
// import UserServices from '../services/UserServices';
// import { userSample } from './sampleTestCase';
// import DbSingleton from '../config/database';
//
// let db = DbSingleton.getInstance();
// let user = userSample;
//
// describe('Function getRolesByUserId', () => {
//     it('#Get roles by user id success', async () => {
//         const userTest = await UserServices.createUser(user);
//         if(userTest !== null) user = {...user, id: userTest.id};
//         await db.query("INSERT INTO users_roles (userid, roleid, rolecode, granter) VALUES ($1, $2, $3, $4)", [user.id, 1, "SA", 1]);
//
//         const result = await RoleServices.getRolesByUserId(user.id);
//         expect(result).to.be.an('array').not.empty;
//     });
//     it('#Get roles by user id fail', async () => {
//         const result = await RoleServices.getRolesByUserId(-1);
//         expect(result).to.be.an('array').empty;
//     });
// });
//
// describe('Function haveRequiredPrivilege', () => {
//     it('#Check if user\'s role for the result is acceptable', async () => {
//         const userRoles = await RoleServices.getRolesByUserId(user.id);
//         const result = RoleServices.haveRequiredPrivilege(userRoles, "SA");
//         expect(result).to.be.true;
//     });
//     it('#Check if user\'s role for the result is not acceptable', async () => {
//         const userRoles = await RoleServices.getRolesByUserId(user.id);
//         const result = RoleServices.haveRequiredPrivilege(userRoles, "EMP");
//         expect(result).to.be.false;
//
//         await db.query("DELETE FROM users_roles WHERE userid = $1", [user.id]);
//         await db.query("DELETE FROM users WHERE id = $1", [user.id]);
//     });
// });
