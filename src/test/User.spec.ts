// import {expect} from 'chai';
// import UserServices from '../services/UserServices';
// import {userSample} from './sampleTestCase';
// import {PoolClient} from "pg";
// import DatabaseCommand from "../utils/DatabaseCommand";
//
// let user = userSample;
//
// let client: PoolClient;
//
// describe("User services Test", () => {
//   before(async function () {
//     client = await DatabaseCommand.newClient();
//     await DatabaseCommand.beginTransaction(client);
//   });
//
//   after(async function () {
//     await DatabaseCommand.rollbackTransaction(client);
//     client.release();
//   });
//
//   try {
//     it("#Create user success", async () => {
//       const result = await UserServices.createUser(user, client);
//       expect(result).to.be.an('object');
//       expect(result?.email).equal(user.email);
//       if (result !== null) user = {...user, id: result.id};
//     });
//
//     it("#Get user by Id and CompanyId success", async () => {
//       const results = await UserServices.getUsers([{id: user.id, companyid: user.companyid}], undefined, client);
//       expect(results).to.be.an('array').not.empty;
//       expect(results[0].email).equal(user.email);
//     });
//
//     it("#Get user by Id and CompanyId fail", async () => {
//       const results = await UserServices.getUsers([{id: - user.id, companyid: user.companyid}], undefined, client);
//       expect(results).to.be.an('array').empty;
//     });
//
//     it('#Update or save user success', async () => {
//       const result = await UserServices.updateOrSaveUser({
//         ...user,
//         address: "TPHCM",
//       }, client);
//       expect(result).to.be.an('object');
//       expect(result?.address).equal('TPHCM');
//     });
//
//     it('#Update or save user fail', async () => {
//       const result = await UserServices.updateOrSaveUser({
//         ...user,
//         address: "TPHCM",
//         id: -1
//       }, client);
//       expect(result).to.be.null;
//     });
//
//     it('#Get all user success', async () => {
//       const result = await UserServices.getAllUser(undefined, client);
//       expect(result).to.be.an('array').not.empty;
//     });
//   } catch (e) {
//     console.log(e)
//     DatabaseCommand.rollbackTransaction(client).then(result => {
//       client.release();
//     });
//   }
// });
